# Smoothing decision borders of neural models
```Engineering thesis about improving generalization of Artificial Intelligence models```

## Short description
The main objectives of this work were to develop a new way to prevent overfitting of Artificial
Intelligence models in the classification process of supervised learning and to develop a way to visualize
their decision borders. This method is based on a parameterized method of extracting points following courses 
of decision functions, approximating these points using polynomials of various degrees and creating a new 
model based on approximating functions. In addition, various methods of visualizing decision functions and 
surfaces of artificial intelligence models using multidimensional data were developed. Despite the
name of the method, it is also suitable for all kinds of artificial intelligence models, that based on input
data assign them a specific label.

## Software usage
Image below shows an example of the effects of the software usage with generated *moons* dataset with comparison
to big, overfitted model (teacher) and distillation student model.

![Decision borders comparison](.gitlab/img/make_moons_decision_borders_comparison.png)

## Engineering thesis
Thesis in which described whole process of decision borders smoothing, its experiments and implementation 
was created and attached to the project in PDF. For this moment the only language that thesis is available 
is polish - *"PL_ProjektDyplomowyInzynierski.pdf"*. Maybe in the future authors will decide to translate whole 
document to English. Thesis was created in parallel with many coworkers using Overleaf website.

![Gdańsk Uniwersity of Technology logo](.gitlab/img/pg_en_poziome_logo_biale.png)

Thesis was created for the purpose of completing the first degree of studies at the 
Gdańsk University of Technology, ETI departments. Therefore, the project was created in accordance 
with the requirements of the university and has license rights defined also by the university.


## Example of achieved experiments results
Example of accuracies achieved by different models using different datasets were presented in table below.

| Dataset   | Teacher | Student | Smoothed decision borders model | Used polynomial degree |
|:----------|:--------|:--------|:--------------------------------|:-----------------------|
| Moon      | 95,3%   | 95,4%   | 95,2%                           | 15                     |
| Blobs     | 96,3%   | 96,7%   | 96,5%                           | 6                      |
| Iris      | 83,4%   | 81,6%   | 82,8%                           | 3                      |
| Banknotes | 93,6%   | 92,6%   | 93,2%                           | 21                     |

--- 

Each of models used during experiments was tested for memory usage after they were saved. 
Scores of created models using different datasets was presented in table below.

| Dataset   | Teacher | Student | Smoothed decision borders model |
|:----------|:--------|:--------|:--------------------------------|
| Moon      | 2075 KB | 76KB    | 52 KB                           |
| Blobs     | 7095 KB | 64 KB   | 64 KB                           |
| Iris      | 516 KB  | 84 KB   | 52 KB                           |
| Banknotes | 2253 KB | 116 KB  | 53 KB                           |

## Technology used
- python 3.10
  - numpy
  - matplotlib
  - tensorflow
  - scikit-learn
  - mlxtend
  - pytest

## License & copyright
Defined by Gdańsk University of Technology. However, project was created of being Open Source since the beginning.
