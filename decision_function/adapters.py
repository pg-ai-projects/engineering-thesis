from typing import Union, Iterable

import numpy as np
import tensorflow as tf
import sklearn.pipeline


class SigmoidAIModel(object):
    """ Class making easier labels prediction made
    by model which returns its belonging probabilities.
    That class was created only for models which
    returns probability of being one class
    among two - model with sigmoid activation function in the last layer.
    """

    def __init__(self, model: tf.keras.models.Sequential, labels: Union[list[int], tuple[int], np.ndarray] = None):
        """ Create SigmoidAIModel object with provided parameters.

        Parameters
        ----------
        model : tf.keras.models.Sequential
            Trained AI model created using Sequential keras model.

        labels : Optional. Union[list[int], tuple[int], np.ndarray]. Default = None.
            List of labels that provided `model` could return.
            Could be None. Then self created indexes represents labels are used.
        """
        self._model = model
        self._labels = labels
        self._validate_state()

    def predict(self, x: np.ndarray):
        """ Predict probabilities of belonging to AI model labels.

        Parameters
        ----------
        x : np.ndarray
            Dataset for prediction.
        """
        if len(x.shape) != 2:
            raise ValueError(f'Invalid shape of x array. Should be only (None, None), not {x.shape}')

        probabilities = self._model.predict(x)
        return self.fit_to_class(probabilities)

    def fit_to_class(self, prob: np.ndarray):
        """ Fit provided probability to the model labels.

        Parameters
        ----------
        prob : Union[float, list[float]]
            Probability of array of probabilities.

        Returns
        -------
        label : np.ndarray
            Label of provided prob
        """
        if not isinstance(prob, np.ndarray):
            raise TypeError(f'Invalid type of probabilities array. Should be ndarray, not {type(prob)}')

        def set_label(probability: np.ndarray):
            idx = 0 if probability <= 0.5 else 1
            if self._labels is None or len(self._labels) != 2:
                return idx
            return self._labels[idx]

        if len(prob.shape) == 1:
            return set_label(prob)

        if len(prob.shape) == 2:
            return np.array(list(map(set_label, prob)))

        raise TypeError(f'Invalid shape of prob')

    def _validate_state(self):
        if self._model is None or not isinstance(self._model, tf.keras.models.Sequential):
            raise TypeError(f'Invalid type of model. Has to be Sequential, not {type(self._model)}')

        if not self._model.layers[-1].activation == tf.keras.activations.sigmoid:
            raise TypeError(f'Invalid activation function of the last model layer. '
                            f'Has to be sigmoid, not {type(self._model.layers[-1].activation)}')


class SoftmaxAIModel(object):
    """ Class making easier labels prediction made
    by model which returns its belonging probabilities.
    That class was created only for models which
    returns probability of being one class
    among the others - model with softmax activation
    function in the last layer.
    """

    def __init__(self, model: tf.keras.models.Sequential, labels: Union[list[int], tuple[int], np.ndarray] = None):
        """ Create SigmoidAIModel object with provided parameters.

        Parameters
        ----------
        model : tf.keras.models.Sequential
            Trained AI model created using Sequential keras model.

        labels : Optional. Union[list[int], tuple[int], np.ndarray]. Default = None.
            List of labels that provided `model` could return.
            Could be None. Then self created indexes represents labels are used.
        """
        self._model = model
        self._labels = labels
        self._validate_state()

    def predict(self, x: np.ndarray):
        """ Predict probabilities of belonging to AI model labels.

        Parameters
        ----------
        x : np.ndarray
            Dataset for prediction.
        """
        if len(x.shape) != 2:
            raise ValueError(f'Invalid shape of x array. Should be only (None, None), not {x.shape}')

        probabilities = self._model.predict(x)
        return self.fit_to_class(probabilities)

    def fit_to_class(self, probs: np.ndarray):
        """ Fit provided probability to the model labels.

        Parameters
        ----------
        probs : Union[float, list[float]]
            Probability of array of probabilities.

        Returns
        -------
        label : Union[str, int]
            Label of provided probs.
        """
        if not isinstance(probs, np.ndarray):
            raise TypeError(f'Invalid type of probabilities array. Should be ndarray, not {type(probs)}')

        def set_label(probabilities: np.ndarray):
            max_idx = np.where(probabilities == np.max(probabilities))[0][0]
            if self._labels is None or len(self._labels) < probabilities.shape[0]:
                return max_idx
            return self._labels[max_idx]

        if len(probs.shape) == 1:
            return set_label(probs)

        if len(probs.shape) == 2:
            return np.array(list(map(set_label, probs)))

        raise TypeError(f'Invalid shape of prob. Could not be {type(probs.shape)}')

    def _validate_state(self):
        if self._model is None or not isinstance(self._model, tf.keras.models.Sequential):
            raise TypeError(f'Invalid type of model. Has to be Sequential, not {type(self._model)}')

        if not self._model.layers[-1].activation == tf.keras.activations.softmax:
            raise TypeError(f'Invalid activation function of the last model layer. '
                            f'Has to be softmax, not {type(self._model.layers[-1].activation)}')


class PolynomialFunction(object):
    """ Class represents approximated polynomial function
    capable of being called as normal function. """

    def __init__(self, model: sklearn.pipeline.Pipeline, points_class: Union[int, None] = None, inequalities: Union[dict, None] = None):
        """ Initialize PolynomialFunction object
        with provided parameters.

        Parameters
        ----------
        model : sklearn.pipeline.Pipeline
            Pipeline represents approximated function with all coefficients and prediction method.
            Created using sklearn library.
        """
        self._model = model
        self._points_class = points_class
        self._ineqs = inequalities
        self._fitted_shape = (None, model.n_features_in_)

    def __call__(self, x: Union[np.ndarray, list, tuple]):
        x = self._proceed_prediction_data(x)
        return self._model.predict(x)

    def fit(self, x, y=None):
        """ Fit Polynomial Function to training data.

        Parameters
        ----------
        x : ndarray
            X learning data as ndarray object.

        y : NOT USED
            Only for convention.
        """
        if len(x.shape) > 2:
            raise ValueError('Invalid shape of x input array.')

        if len(x.shape) == 1:
            x = x[np.newaxis, :]

        self.x_ = x
        self._x_maxes_ = np.amax(self.x_, axis=0)[:-1].astype(np.float32)
        self._x_mins_ = np.amin(self.x_, axis=0)[:-1].astype(np.float32)
        vectors = [np.arange(axis_min, axis_max, 0.01)
                   for axis_max, axis_min in zip(self._x_maxes_, self._x_mins_)]
        grids = np.meshgrid(*vectors)
        self.ds_ = np.array(list(map(lambda array: array.ravel(), grids))).T
        self.ds_ = np.concatenate((self.ds_, self.predict(self.ds_)[np.newaxis, :].T), axis=1)
        return self

    def predict(self, x: Union[np.ndarray, list, tuple]):
        """ Make prediction on provided data ndarray.

        Parameters
        ----------
        x : ndarray
            Data on which prediction should be done.

        Returns
        -------
        values : ndarray
            Array of predictions.
        """
        x = self._proceed_prediction_data(x)
        return self._model.predict(x)

    def is_class(self, x: np.ndarray):
        """ Decide are provided input array elements belongs to
        function decision area.

        Parameters
        ----------
        x : ndarray
            Array of points to decide.

        Returns
        -------
        classes : ndarray[bool]
            Array of bool values define is provided point belongs to
            function decision are or not.
        """
        if self._points_class is None:
            raise ValueError('It is impossible to classify points to unknown class. '
                             'You has to set points_class parameter to use that functionality.')

        if self._ineqs is None or len(self._ineqs) <= 0:
            raise ValueError('It is impossible to check is input data class of function or not '
                             'because of inequalities parameter is None or empty dict')

        if len(x.shape) > 2:
            raise ValueError('Invalid shape of x input array.')

        if len(x.shape) == 1:
            x = x[np.newaxis, :]

        points = x.copy()
        points[:, -1] = self.predict(points[:, :-1])
        x_is_class = np.full(x.shape[0], True)
        # indexes = list(range(x.shape[1]))

        for i, xx in enumerate(x):
            # diffs = np.abs(self.ds_ - xx)
            for dim_idx, ineq in self._ineqs.items():

                comp_point = points[i]
                # comp_point = self.ds_[np.argmin(np.sum(diffs[:, indexes[:dim_idx]+indexes[dim_idx + 1:]], axis=1))]

                if ineq == '>':
                    x_is_class[i] = x_is_class[i] and (xx[dim_idx] >= comp_point[dim_idx])
                elif ineq == '<':
                    x_is_class[i] = x_is_class[i] and (xx[dim_idx] <= comp_point[dim_idx])

        return x_is_class

    @property
    def points_class(self):
        return self._points_class

    def _proceed_prediction_data(self, data: Union[np.ndarray, list, tuple]):
        if not isinstance(data, (np.ndarray, list, tuple)):
            raise TypeError(f'Incorrect type of data parameter. Could not be {type(data)}')

        if isinstance(data, (list, tuple)):
            data = np.array(data)

        if data.dtype.type not in (np.int8, np.int16, np.int32, np.int64,
                                   np.float16, np.float32, np.float64):
            raise TypeError(f'Incorrect type of element(s) of data parameter.')

        if len(data.shape) <= 1:
            data = data[:, np.newaxis]

        if len(data.shape) != len(self._fitted_shape):
            raise TypeError(f'Shape of data used for fitting is different than used to prediction. '
                            f'Fitting was {len(self._fitted_shape)}dim and provided is {len(data.shape)}dim')

        return data

    def _validate_state(self):
        if not isinstance(self._model, sklearn.pipeline.Pipeline):
            raise TypeError(f'Invalid type of model parameter. Has to be sklearn.pipeline.Pipeline not {type(self._model)}.')

        if not hasattr(self._model, 'predict'):
            raise NotImplementedError(f'Provided model does not contain a function "predict()".')

        if not callable(getattr(self._model, 'predict', None)):
            raise NotImplementedError(f'Provided model "predict()" function is not callable.')

        if not isinstance(self._points_class, Union[dict, None]):
            raise TypeError(f'Invalid type of points_class parameter. Has to be int or None not {type(self._points_class)}.')

        if not isinstance(self._ineqs, Union[dict, None]):
            raise TypeError(f'Invalid type of inequalities parameter. Has to be dict or None not {type(self._ineqs)}.')


class DecisionFunctionsAIModel(object):
    """ Artificial Intelligence model based on set of decision
    functions created during decision boundaries smoothing process.
    """

    def __init__(self, functions: Iterable[PolynomialFunction]):
        """ Initialize DecisionFunctionsAIModel object
        with provided parameters.

        Parameters
        ----------
        functions : Iterable[PolynomialFunction]
            Set of PolynomialFunction objects that represent model decision functions.
        """
        self._functions = functions
        self._validate_state()

    def predict(self, x: np.ndarray):
        """ Predict class(es) of provided points.

        Parameters
        ----------
        x : ndarray
            Array of points on which prediction should be launched.

        Returns
        -------
        y : ndarray
            Array of classes with length equal to number of `x` parameter elements.
        """
        if not isinstance(x, np.ndarray):
            raise TypeError(f'Invalid type of "x" parameter. Could not be {type(x)}')

        if len(x.shape) != 2:
            raise ValueError(f'Invalid shape of provided "x" parameter. Could be only (None, None), not {x.shape}')

        y = np.full(x.shape[0], np.nan)
        for func in self._functions:
            func_classes = func.is_class(x)
            points_ids = np.where(func_classes)
            y[points_ids] = func.points_class
        return y

    def _validate_state(self):
        if not isinstance(self._functions, (list, tuple, Iterable, np.ndarray)):
            raise TypeError(f'Invalid type of provided functions parameter. Could not be {type(self._functions)}')

        for func in self._functions:
            if not isinstance(func, PolynomialFunction):
                raise TypeError(f'Invalid type of provided functions parameter element(s). Could not be {type(func)}')