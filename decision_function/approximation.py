import matplotlib.pyplot as plt
import tensorflow as tf
import numpy as np
import warnings
from typing import Union

from sklearn.preprocessing import StandardScaler, PolynomialFeatures
from sklearn.linear_model import Ridge
from sklearn.pipeline import make_pipeline
from sklearn.datasets import make_moons
from mlxtend.plotting import plot_decision_regions

from decision_function.adapters import SigmoidAIModel, SoftmaxAIModel, PolynomialFunction
from decision_function.extraction import DecisionFunctionExtractor


class FunctionApproximator(object):
    """ Class capable of function approximation
    represents as set of points.

    Attributes
    ----------
    _degree : int
        Degree of polynomial approximated function.

    _alpha : Union[float, int]
        Value multiplies the L2 term, controlling regularization strength.

    _fitted : bool
        Boolean variable contains information about fitness of object.

    model_ : sklearn.pipeline.Pipeline
        Pipeline represents approximated function with all coefficients and prediction method.

    x_ : ndarray
        Fitted x parameter used for calculating polynomial function coefficients.
    """

    def __init__(self, degree: int, alpha: Union[float, int] = 1e-3, points_class: int = None, inequalities: dict = None):
        """ Initialize FunctionApproximator object with provided parameters.

        Parameters
        ----------
        degree : int
            Degree of polynomial being approximation of function.

        alpha : Union[float, int]. Optional, default = 1e-3.
            Value multiplies the L2 term, controlling regularization strength.

        points_class : Optional. Union[int, None]. Default = None.
            Class define to which decision area points belong.

        inequalities : Optional. Union[dict, None]. Default = None.
            Inequalities dictionary defined on which side of axis(es) the classified points should be.
            Element of classes_inequalities returned from DecisionPointsExtractor.
        """
        self._degree = degree
        self._alpha = alpha
        self._ineqs = inequalities
        self._points_class = points_class
        self._fitted = False
        self._validate_state()

    def fit(self, x: np.ndarray, y=None):
        """ Fit object to trained data.

        Parameters
        ----------
        x : ndarray
            Data used to calculating polynomial function coefficients.

        y : NOT USED
            Only for .fit(x, y) convention.

        Returns
        -------
        self : FunctionApproximator
            Trained object.
        """
        if not isinstance(x, np.ndarray):
            raise TypeError(f'Invalid type of x parameter. Could be only ndarray, not {type(x)}')

        if len(x.shape) != 2:
            raise ValueError(f'Invalid shape of x parameter. Has to be (None, None), not {x.shape}')

        self.x_ = x
        self.model_ = make_pipeline(PolynomialFeatures(degree=self._degree), Ridge(alpha=self._alpha))
        self._fitted = True

        if not self._is_data_standardized(x):
            warnings.warn('Data used for fitting is not standardized. Function approximation process '
                          'is significantly more predictable and easier to control.')
        return self

    def approximate(self):
        """ Approximate function represents as fitted data as
        polynomial function with provided degree.

        Returns
        -------
        polynomial : PolynomialFunction
            Function object ready to call as normal callable function.
        """
        if not self._fitted:
            raise PermissionError(f'Model has not been fitted yet. It is needed before approximation process.')

        self.model_.fit(self.x_[:, :-1], self.x_[:, -1])
        pf = PolynomialFunction(self.model_, self._points_class, self._ineqs)
        return pf.fit(self.x_)

    def _is_data_standardized(self, data: np.ndarray):
        """ Is provided data standardized.

         Parameters
         ----------
         data : ndarray
            Data to check.

         Returns
         -------
         standardized : bool
            Is provided `data` standardized or not.
         """
        if data is None or not isinstance(data, np.ndarray):
            return False

        data_std = StandardScaler().fit_transform(data)
        return (data.astype(np.float32) == data_std.astype(np.float32)).all()

    def _validate_state(self):
        if not isinstance(self._degree, int):
            raise TypeError(f'Invalid type of degree parameter. Has to be int not {type(self._degree)}.')

        if self._degree < 0:
            raise ValueError(f'Invalid value of degree parameter. Has to be greater than or equal to 0.')

        if not isinstance(self._alpha, (float, int)):
            raise TypeError(f'Invalid type of alpha parameter. Has to be float not {type(self._alpha)}.')

        if not isinstance(self._points_class, Union[int, None]):
            raise TypeError(f'Invalid type of points_class parameter. Has to be int or None not {type(self._points_class)}.')

        if not isinstance(self._ineqs, Union[dict, None]):
            raise TypeError(f'Invalid type of inequalities parameter. Has to be dict or None not {type(self._ineqs)}.')


if __name__ == '__main__':
    moons = make_moons(n_samples=500, noise=.2, random_state=1)
    x_moons = StandardScaler().fit_transform(moons[0])
    y_moons = moons[1]

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(128, activation='relu'))
    model.add(tf.keras.layers.Dense(256, activation='relu'))
    model.add(tf.keras.layers.Dense(512, activation='relu'))
    model.add(tf.keras.layers.Dense(2, activation='softmax'))
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.fit(x_moons, y_moons, epochs=100)
    model.summary()

    # ====================================
    # softmax example
    extraction = DecisionFunctionExtractor(model, precision=0.01).fit(x_moons, y_moons).extract()

    # ==========================================================================
    # Comparison of regularization strength and different degrees of polynomials
    fig, axs = plt.subplots(1, 2, sharey='row')
    plot_decision_regions(x_moons, y_moons, SoftmaxAIModel(model), ax=axs[0], legend=False)
    for clazz, points in extraction.classes_points.items():
        if len(points) > 0:
            inequalities = extraction.classes_inequalities.get(clazz, None)
            axs[0].scatter(points[:, 0], points[:, 1], alpha=0.1, label='Wyekstrahowane punkty')
            xs = np.arange(-3, 4, 0.01)
            for alpha, ls in zip([1e-2, 1e-1, 1, 10], ['-', '--', '-.', ':']):
                func = FunctionApproximator(degree=10, alpha=alpha, points_class=clazz, inequalities=inequalities).fit(points).approximate()
                axs[0].plot(xs, func(xs), linewidth=3, linestyle=ls, label=f'gamma = {alpha}')
            break
    axs[0].legend()
    axs[0].set_title('Wpływ wartości współczynnika regularyzacji na efekt aproksymacji. Stopień wielomianu = 10')
    axs[0].set_xlim([-2.5, 2.5])
    axs[0].set_ylim([-2.5, 3])
    handles, labels = axs[0].get_legend_handles_labels()
    axs[0].legend(handles, ['Punkty klasy A', 'Punkty klasy B', 'Wyekstrahowane punkty',
                            'gamma = 0.01', 'gamma = 0.1', 'gamma = 1.0', 'gamma = 10.0'])

    plot_decision_regions(x_moons, y_moons, SoftmaxAIModel(model), ax=axs[1], legend=False)
    for clazz, points in extraction.classes_points.items():
        if len(points) > 0:
            axs[1].scatter(points[:, 0], points[:, 1], alpha=0.1, label='Wyekstrahowane punkty')
            xs = np.arange(-3, 4, 0.01)
            for deg, ls in zip([1, 3, 5, 7], ['-', '--', '-.', ':']):
                func = FunctionApproximator(degree=deg, alpha=0.001).fit(points).approximate()
                axs[1].plot(xs, func(xs), linewidth=3, linestyle=ls, label=f'Stopien = {deg}')
            break
    axs[1].legend()
    axs[1].set_title('Wpływ stopnia wielomianu na efekt aproksymacji. Gamma = 0.001')
    axs[1].set_xlim([-2.5, 2.5])
    axs[1].set_ylim([-2.5, 3])
    handles, labels = axs[1].get_legend_handles_labels()
    axs[1].legend(handles, ['Punkty klasy A', 'Punkty klasy B', 'Wyekstrahowane punkty',
                            'Stopień = 1', 'Stopień = 3', 'Stopień = 4', 'Stopień = 7'])

    plt.tight_layout()
    plt.show()

    # ===================================================================
    # Correct approximation example
    ax = plot_decision_regions(x_moons, y_moons, SoftmaxAIModel(model))
    for clazz, points in extraction.classes_points.items():
        if len(points) > 0:
            plt.scatter(points[:, 0], points[:, 1], alpha=0.1)
            xs = np.arange(-3, 4, 0.01)
            func = FunctionApproximator(degree=11, alpha=0.01).fit(points).approximate()
            plt.plot(xs, func(xs), linewidth=3, label=f'Aproksymowana funkcja wielomianowa', c='purple')
            break
    plt.legend()
    plt.title('Przykład wygładzenia funkcji decyzyjnej. Gamma = 0.01, stopień wielomianu = 11')
    plt.xlim([-2.5, 2.5])
    plt.ylim([-2.5, 3])
    handles, labels = ax.get_legend_handles_labels()
    ax.legend(handles, ['Punkty klasy A', 'Punkty klasy B', 'Funkcja aproksymująca'])
    plt.tight_layout()
    plt.show()

    # ===================================================================
    # Sigmoid model example
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(128, activation='relu'))
    model.add(tf.keras.layers.Dense(128, activation='softmax'))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    model.fit(x_moons, y_moons, epochs=50)
    model.summary()

    points = DecisionFunctionExtractor(model, precision=0.01).fit(x_moons, y_moons).extract().classes_points[0]

    plot_decision_regions(x_moons, y_moons, SigmoidAIModel(model), legend=False)
    plt.scatter(points[:, 0], points[:, 1], alpha=0.1, label='Wyekstrahowane punkty')
    xs = np.arange(-3, 4, 0.01)
    for alpha, ls in zip([1e-2, 1e-1, 1, 10], ['-', '--', '-.', ':']):
        func = FunctionApproximator(degree=10, alpha=alpha).fit(points).approximate()
        plt.plot(xs, func(xs), linewidth=3, linestyle=ls, label=f'gamma = {alpha}')

    plt.legend()
    plt.title('Wpływ wartości współczynnika regularyzacji na efekt aproksymacji. Stopień wielomianu = 10')
    plt.xlim([-2.5, 2.5])
    plt.ylim([-2.5, 3])
    plt.show()
