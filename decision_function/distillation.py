from typing import Union

import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.datasets import make_moons
from sklearn.preprocessing import StandardScaler

from decision_function import DecisionFunctionExtractor
from decision_function.approximation import FunctionApproximator
from decision_function.generation import GridGenerator


class KnowledgeDistillator(object):
    """ Class responsible for creating and teaching a student model
     based on approximation functions.
    """

    def __init__(self, dense_units: Union[list[int], tuple[int], np.ndarray],
                 activations: Union[list[str], tuple[str], np.ndarray],
                 epochs: int,
                 grid_generator: GridGenerator):
        """ Initialize the KnowledgeDistillator with provided parameters.

        Parameters
        ----------
        dense_units : Union[list[int], tuple[int], np.ndarray]
            Number of neurons used for creating a student model.

        activations : Union[list[str], tuple[str], np.ndarray]
            Activation functions used for creating a student model.

        epochs : int
            Number of epochs used for teaching a student model.

        grid_generator : GridGenerator
            GridGenerator used for creating points grid for teaching a student model.
        """
        self._dense_units = np.array(dense_units)
        self._activations = np.array(activations)
        self._epochs = epochs
        self._grid_generator = grid_generator
        self._validate_state()

        self._student = self._make_student_model()
        self._fitted = False

    def _make_student_model(self):
        """ Make a student model.

        Returns
        -------
        student_model : tf.keras.models.Sequential
            Made student model using dense_units and activations.
        """
        student_model = tf.keras.models.Sequential()
        for units, activation in zip(self._dense_units, self._activations):
            student_model.add(tf.keras.layers.Dense(units, activation))
        if student_model.layers[-1].activation.__name__ == "sigmoid":
            student_model.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
        else:
            student_model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
        return student_model

    def fit(self, X, y=None):
        """ Fit a student model.

        Parameters
        ----------
        X : np.ndarray
            Data needed to count extreme points of the example data set.

        y : None, Not used
            Only for .fit() function convention.

        Returns
        -------
        self : KnowledgeDistillator
            KnowledgeDistillator with student model taught based on approximation functions.
        """
        self._fitted = True

        x_min, x_max = min(X[:, 0]) - 1, max(X[:, 0]) + 1
        y_min, y_max = min(X[:, 1]) - 1, max(X[:, 1]) + 1
        x_train = self._grid_generator.make_grid(x_min, x_max, y_min, y_max)
        x_train, y_train = self._classify_grid_elements(x_train)

        self._student.fit(x_train, y_train, epochs=self._epochs)
        return self

    def _classify_grid_elements(self, grid: np.ndarray):
        """ Classify points grid to classes
         pointed by approximation functions.

        Parameters
        ----------
        grid : np.ndarray
            Points grid to classify.

        Returns
        -------
        grid_classes : np.asarray
            Classes corresponding to points in the given grid.
        grid: np.asarray
            Points corresponding to grid_classes.
        """
        approx_funcs = self._grid_generator.approx_funcs
        grid_classes = np.full(grid.shape[0], np.nan)

        for approx_func in approx_funcs:
            func_classes = approx_func.is_class(grid)
            points_ids = np.where(func_classes)
            grid_classes[points_ids] = approx_func.points_class

        not_nan = np.logical_not(np.isnan(grid_classes))

        return grid[not_nan], grid_classes[not_nan]

    @property
    def student(self):
        if self._fitted:
            return self._student
        raise PermissionError('Student model has not been fitted yet. Make this before student property usage.')

    def _validate_state(self):
        if len(self._dense_units) != len(self._activations):
            raise ValueError('Lengths of dens_units and activations parameters are different.')

        if self._dense_units.dtype.type not in (np.int8, np.int16, np.int32, np.int64):
            raise TypeError(f'Invalid type of dense_units. Could not be {self._dense_units.dtype.type}')

        if self._activations.dtype.type.__name__ != 'str_':
            raise TypeError(f'Invalid type of activations. Could not be {self._activations.dtype.type}')

        if self._activations[-1] != "sigmoid" and self._activations[-1] != "softmax":
            raise ValueError(f"The last activation function must be sigmoid or softmax. Could not be {self._activations[-1]}")

        if not isinstance(self._epochs, int):
            raise TypeError(f'Invalid type of epochs. Could not be {type(self._epochs)}')

        if not isinstance(self._grid_generator, GridGenerator):
            raise TypeError(f'Invalid type of grid_generator. Could not be {type(self._grid_generator)}')


if __name__ == '__main__':
    moons = make_moons(n_samples=500, noise=.2, random_state=1)
    x_moons = StandardScaler().fit_transform(moons[0])
    y_moons = moons[1]

    teacher = tf.keras.models.Sequential()
    teacher.add(tf.keras.layers.Dense(16, activation='relu'))
    teacher.add(tf.keras.layers.Dense(48, activation='relu'))
    teacher.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    teacher.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    teacher.fit(x_moons, y_moons, epochs=100)

    extraction = DecisionFunctionExtractor(teacher, precision=0.1).fit(x_moons, y_moons).extract()

    funcs = []
    for clazz, points in extraction.classes_points.items():
        clazz = int(clazz)
        funcs.append(
            FunctionApproximator(degree=11,
                                 alpha=0.01,
                                 points_class=clazz,
                                 inequalities=extraction.classes_inequalities[clazz]).fit(points).approximate()
        )

    for func in funcs:
        xs = np.arange(-3, 3, 0.01)
        plt.scatter(xs, func(xs))
    plt.ylim([-3, 3])
    plt.show()
    kd = KnowledgeDistillator([8, 24, 2], ['relu', 'relu', 'softmax'], 100, GridGenerator(funcs, 500, 'tunnel'))

    student = kd.fit(x_moons).student

    predictions_student = student.predict(x_moons[:500])

    error_student = 0

    for i in range(len(predictions_student)):
        guess = np.argmax(predictions_student[i])
        actual = y_moons[i]
        if guess != actual:
            error_student += 1

    print("\nStudent Accuracy = " + str(len(predictions_student) - error_student) + "/" + str(len(predictions_student)))
    print("\nStudent Accuracy = " + str(
        ((len(predictions_student) - error_student) / len(predictions_student)) * 100) + "%")

    predictions_teacher = teacher.predict(x_moons[:500])

    error_teacher = 0

    for i in range(len(predictions_teacher)):
        guess = np.argmax(predictions_teacher[i])
        actual = y_moons[i]
        if guess != actual:
            error_teacher += 1

    print("\nTeacher Accuracy = " + str(len(predictions_teacher) - error_teacher) + "/" + str(len(predictions_teacher)))
    print("\nTeacher Accuracy = " + str(
        ((len(predictions_teacher) - error_teacher) / len(predictions_teacher)) * 100) + "%")
