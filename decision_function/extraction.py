from typing import Union
import warnings
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt

from mlxtend.plotting import plot_decision_regions
from sklearn.preprocessing import StandardScaler

from decision_function.adapters import SigmoidAIModel, SoftmaxAIModel


class DecisionFunctionExtractor(object):
    """ Class responsible for extraction points
    of decision function from Artificial Intelligence model.
    """

    def __init__(self, model: tf.keras.models.Sequential, precision: float = 0.01):
        """ Initialize the DecisionFunctionExtractor with provided parameters

        Parameters
        ----------
        model : tf.keras.models.Sequential
            AI model created using Sequential class.

        precision : float optional. Default = 0.01
            Precision of decision function points extraction.
        """
        self._model = model
        self._precision = precision
        self._fitted = False
        self._classes = None
        self._validate_state()

    def fit(self, x: np.ndarray, y: np.ndarray):
        """ Fit extractor to learning dataset.

        Parameters
        ----------
        x : np.ndarray
            Learning examples.

        y : np.ndarray
            Labels of learning examples.

        standardize : bool optional. Default = False.
            Should the `x` data be standardize.

        Returns
        -------
        extractor : DecisionFunctionExtractor
            Fitted decision function extractor.
        """
        if x is None or not isinstance(x, np.ndarray):
            raise TypeError(f'Invalid type of x parameter. Could be only ndarray, not {type(x)}')

        if y is None or not isinstance(y, np.ndarray):
            raise TypeError(f'Invalid type of y parameter. Could be only ndarray, not {type(y)}')

        if len(x.shape) != 2:
            raise ValueError(f'Invalid shape of x parameter. Has to be (None, None), not {x.shape}')

        if len(y.shape) != 1:
            raise ValueError(f'Invalid shape of x parameter. Has to be (None, None), not {y.shape}')

        if not self._is_data_standardized(x):
            warnings.warn('Data used for fitting is not standardized. Decision function extraction process '
                          'is significantly slower without standardization.')

        self.x_ = x
        self.y_ = y
        self._classes = np.unique(self.y_)
        self.aimodel_ = self._set_ai_model_adapter()
        self._fitted = True
        return self

    def extract(self, max_offset: Union[int, float] = 1, min_offset: Union[int, float] = 1):
        """ Extract AI model decision function
        points with defined precision.

        Parameters
        ----------
        max_offset : Union[int, float] optional. Default = 1.
            Offset between maximum value of fitted `x` and
            meshgrid that will be created for extraction.

        min_offset : Union[int, float] optional. Default = 1.
            Offset between maximum value of fitted `x` and
            meshgrid that will be created for extraction.
        """
        if not self._fitted:
            raise PermissionError(f'Model has not been fitted yet. It is needed before extraction process.')

        if not isinstance(max_offset, (int, float)):
            raise TypeError(f'Invalid type of max_offset parameter. Could not be {type(max_offset)}')

        if not isinstance(min_offset, (int, float)):
            raise TypeError(f'Invalid type of max_offset parameter. Could not be {type(min_offset)}')

        self._x_maxes_ = np.amax(self.x_, axis=0).astype(np.float32) + max_offset
        self._x_mins_ = np.amin(self.x_, axis=0).astype(np.float32) - min_offset
        vectors = [np.arange(axis_min, axis_max, self._precision)
                   for axis_max, axis_min in zip(self._x_maxes_, self._x_mins_)]
        grids = np.meshgrid(*vectors)
        self.ds_ = np.array(list(map(lambda array: array.ravel(), grids))).T

        if isinstance(self.aimodel_, SoftmaxAIModel):
            classes_points, classes_inequalities = self._softmax_extract(*grids)
        elif isinstance(self.aimodel_, SigmoidAIModel):
            classes_points, classes_inequalities = self._sigmoid_extract(*grids)
        else:
            raise NotImplementedError('Not implemented type of AI model.')
        return ExtractedPoints(classes_points, classes_inequalities)

    def _sigmoid_extract(self, *grids):
        """ Extract decision function points
        from sigmoid AI model.

        Parameters
        ----------
        *grids : tuple
            grids returned by meshgrid numpy function.

        Returns
        -------
        points : np.ndarray
            Decision function points
        """
        z = self.aimodel_.predict(self.ds_)
        z_grid = z.reshape(grids[0].shape)

        points, inequalities = {}, {}
        for unique_label in np.unique(z):
            points[unique_label] = self._extract_points_with_different_labels(z_grid, *grids)
            inequalities[unique_label] = self._extract_class_points_inequalities(points[unique_label], unique_label)
        return points, inequalities

    def _softmax_extract(self, *grids):
        """ Extract decision function points
        from sigmoid AI model.

        Parameters
        ----------
        *grids : tuple
            grids returned by meshgrid numpy function.

        Returns
        -------
        points : dict[int, np.ndarray]
            Dictionary of decision functions points.
        """
        z = self.aimodel_.predict(self.ds_)
        z_grid = z.reshape(grids[0].shape)

        points, inequalities = {}, {}
        for unique_label in np.unique(z):
            z_grid_sub = z_grid.copy()
            z_grid_sub[z_grid != unique_label] = -(unique_label+1)
            points[int(unique_label)] = self._extract_points_with_different_labels(z_grid_sub, *grids)
            inequalities[int(unique_label)] = self._extract_class_points_inequalities(points[unique_label], unique_label)
        return points, inequalities

    def _extract_points_with_different_labels(self, z_grid: np.ndarray, *grids):
        """ Extract points between points with different labels from provided z_grid.

        Parameters
        ----------
        z_grid: np.ndarray
            Array contains labels of created and predicted points.

        *grids : tuple
            grids returned by meshgrid numpy function.

        Returns
        -------
        points : np.ndarray
            Points between points with different labels.
        """
        no_axes = len(grids[0].shape)

        decision_points = [[] for _ in range(no_axes)]
        for axis in range(no_axes):
            axis_mask = np.diff(z_grid, axis=axis) != 0
            axis_mask1 = np.insert(axis_mask, 0, False, axis=axis)
            axis_mask2 = np.insert(axis_mask, axis_mask.shape[axis], False, axis=axis)

            for sub_axis in range(no_axes):
                mask1_points = grids[sub_axis][axis_mask1]
                mask2_points = grids[sub_axis][axis_mask2]
                decision_points[axis].append(np.insert(mask2_points, np.arange(len(mask1_points)), mask1_points))

            decision_points[axis] = list(zip(*decision_points[axis]))
            decision_points[axis] = np.array(list(zip(decision_points[axis][::2], decision_points[axis][1::2])))
            decision_points[axis] = np.array(list(map(lambda pair: (pair[0] + pair[1]) * 0.5, decision_points[axis])))
        return np.concatenate(decision_points)

    def _extract_class_points_inequalities(self, extracted_points: np.ndarray, unique_label: int, precision: float = 0.75):
        """ Extract function inequalities defines on which side of
        function decision border of unique classes is.

        Notes
        -----
        Inequality is designated using all points belong to extracted_points.
        Based of that points two subsets for each dimension are created. One with value of
        axis increased by `self._precision` step and one with values of axis decreased
        with the same step. After that, prediction is used to change points to the classes.
        Then, number of labels equal to `unique_label` is compared in each subset. Based
        on subsets with more unique_classes the inequality is returned.
        inc_subset -> '>'
        dec_subset -> '<'
        """
        no_extracted_points = len(extracted_points)
        inequalities = {}
        pred_points = np.empty((2*no_extracted_points*len(self.x_.shape), extracted_points.shape[1]))

        for axis in range(len(self.x_.shape)):
            inc_points = extracted_points.copy()
            dec_points = extracted_points.copy()

            inc_points[:, axis] = inc_points[:, axis] + self._precision
            dec_points[:, axis] = dec_points[:, axis] - self._precision

            pred_points[axis*2*no_extracted_points:(axis+1)*2*no_extracted_points] = np.concatenate((inc_points, dec_points), axis=0)

        pred_classes = self.aimodel_.predict(pred_points)
        for axis in range(len(self.x_.shape)):
            inc_classes = pred_classes[axis*2*no_extracted_points:axis*2*no_extracted_points+no_extracted_points]
            dec_classes = pred_classes[axis*2*no_extracted_points+no_extracted_points:axis*2*no_extracted_points+2*no_extracted_points]

            inc_correct = np.sum(inc_classes == unique_label)
            dec_correct = np.sum(dec_classes == unique_label)

            if inc_correct == dec_correct or \
                    (inc_correct <= precision * no_extracted_points and dec_correct <= precision * no_extracted_points):
                continue

            inequalities[axis] = '>' if inc_correct > dec_correct else '<'

        return inequalities

    def _set_ai_model_adapter(self):
        """ Set adapter for Artificial Intelligence
        model predicts labels instead of probabilities.

        Returns
        -------
        ai_model
            Created and initialized adapter of AI model.
        """
        ai_models = {
            tf.keras.activations.sigmoid: SigmoidAIModel,
            tf.keras.activations.softmax: SoftmaxAIModel
        }
        ai_model = ai_models.get(self._model.layers[-1].activation, None)
        if ai_model is None:
            raise NotImplementedError(f'{self._model.layers[-1].activation} is not allowed last layer activation function.')
        return ai_model(self._model, self._classes)

    def _is_data_standardized(self, data: np.ndarray):
        """ Is provided data standardized.

         Parameters
         ----------
         data : ndarray
            Data to check.

         Returns
         -------
         standardized : bool
            Is provided `data` standardized or not.
         """
        if data is None or not isinstance(data, np.ndarray):
            return False

        data_std = StandardScaler().fit_transform(data)
        return (data.astype(np.float32) == data_std.astype(np.float32)).all()

    def _validate_state(self):
        if self._model is None or not isinstance(self._model, tf.keras.models.Sequential):
            raise TypeError(f'Invalid type of model. Could not be {type(self._model)}')

        if not isinstance(self._precision, float):
            raise TypeError(f'Invalid type of precision. Could not be {type(self._precision)}')

        if not self._model.layers[-1].activation == tf.keras.activations.sigmoid and \
                not self._model.layers[-1].activation == tf.keras.activations.softmax:
            raise NotImplementedError(f'Not implemented type of activation function of last model layer')


class ExtractedPoints(object):
    """ Class returned by DecisionFunctionExtractor as
    the result of decision points extraction
    """

    def __init__(self, classes_points: dict, classes_inequalities: dict):
        """ Initialize the ExtractedPoints object.

        Parameters
        ----------
        classes_points : dict
            Dictionary of classes (keys) and their extracted points (values).

        classes_inequalities : dict
            Dictionary of classes (keys) and their inequalities (values) defined on which side of decision function classes are.
        """
        self._classes_points = classes_points
        self._classes_inequalities = classes_inequalities

    @property
    def classes_points(self):
        return self._classes_points

    @property
    def classes_inequalities(self):
        return self._classes_inequalities




if __name__ == '__main__':
    from sklearn.datasets import load_iris
    x, y = load_iris(return_X_y=True)

    # ====================================
    # softmax example

    x_train = StandardScaler().fit_transform(x[:, :2])

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(128, activation='relu'))
    model.add(tf.keras.layers.Dense(3, activation='softmax'))
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    model.fit(x_train, y, epochs=100)
    model.summary()

    classes_points = DecisionFunctionExtractor(model, precision=0.01).fit(x_train, y).extract().classes_points
    plot_decision_regions(x_train, y, SoftmaxAIModel(model))
    for clazz, points in classes_points.items():
        if len(points) > 0:
            plt.scatter(points[:, 0], points[:, 1], alpha=0.1)
    plt.title('Example of softmax decision function extraction. Precision = 0.01')
    plt.show()

    # ===================================
    # sigmoid example

    x_train = StandardScaler().fit_transform(x[y != 2][:, :2])
    y_train = y[y != 2]

    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(50, activation='relu'))
    model.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.fit(x_train, y_train, epochs=100)
    model.summary()

    classes_points = DecisionFunctionExtractor(model, precision=0.1).fit(x_train, y_train).extract().classes_points
    plot_decision_regions(x_train, y_train, model)
    for clazz, points in classes_points.items():
        if len(points) > 0:
            plt.scatter(points[:, 0], points[:, 1])
    plt.title('Example of sigmoid decision function extraction. Precision = 0.1')
    plt.show()
