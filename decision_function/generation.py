from typing import Iterable, Union

import numpy as np

from decision_function.adapters import PolynomialFunction


class GridGenerator(object):
    """ Class responsible for generating points grid."""

    def __init__(self, approx_funcs: Union[PolynomialFunction, Iterable[PolynomialFunction]],
                 n_points: int,
                 grid_strategy: str = 'random'):
        """ Initialize the GridGenerator with provided parameters.

        Parameters
        ----------
        approx_funcs : Union[PolynomialFunction, Iterable[PolynomialFunction]]
            Approximation functions used for generating tunnel points grid.

        n_points: int
            Number of points to generate in the random strategy.

        grid_strategy : str optional. Default = random
            Strategy to generate points grid. Possible strategies: random, tunnel, regular.
        """
        self._approx_funcs = approx_funcs
        self._n_points = n_points
        self._grid_strategy = grid_strategy
        self._validate_state()

    def make_grid(self, x_min: float, x_max: float, y_min: float, y_max: float) -> np.ndarray:
        """ Make a points grid based on a given strategy.

        Parameters
        ----------
        x_min : float
            Starting x of a grid.

        x_max : float
            Ending x of a grid.

        y_min: float
            Starting y of a grid.

        y_max: float
            Ending y of a grid.

        Returns
        -------
        grid : np.ndarray
            Made points grid.
        """
        strategies = {
            'random': self._make_random_grid,
            'tunnel': self._make_tunnel_grid,
            'regular': self._make_regular_grid
        }
        strategy = strategies.get(self._grid_strategy, self._make_regular_grid)

        return strategy(x_min, x_max, y_min, y_max)

    def _make_random_grid(self, x_min: float, x_max: float, y_min: float, y_max: float):
        """ Make a points grid based on the random strategy.

        Parameters
        ----------
        x_min : float
            Starting x of a grid.

        x_max : float
            Ending x of a grid.

        y_min: float
            Starting y of a grid.

        y_max: float
            Ending y of a grid.

        Returns
        -------
        grid : np.ndarray
            Made random points grid.
        """
        x_width, y_width = x_max - x_min, y_max - y_min

        grid = np.random.random((self._n_points, 2))
        grid[:, 0] = grid[:, 0] * x_width + x_min
        grid[:, 1] = grid[:, 1] * y_width + y_min

        return grid

    def _make_tunnel_grid(self, x_min: float, x_max: float, y_min: float, y_max: float):
        """ Make a points grid based on the tunnel strategy.

        Parameters
        ----------
        x_min : float
            Starting x of a grid.

        x_max : float
            Ending x of a grid.

        y_min: float
            Starting y of a grid.

        y_max: float
            Ending y of a grid.

        Returns
        -------
        grid : np.ndarray
            Made tunnel points grid.
        """
        xs = np.arange(x_min, x_max, 0.1)
        ys = np.arange(y_min, y_max, 0.1)

        grid = []
        for x in xs:
            for y in ys:
                grid.append([x, y])
        grid = np.array(grid)

        tunnel_span = 1.25
        grids = None
        for approx_func in self._approx_funcs:
            tunnel_grid = grid

            y_func = approx_func(tunnel_grid[:, 0])
            tunnel_grid = tunnel_grid[tunnel_grid[:, 1] <= y_func + tunnel_span]

            y_func = approx_func(tunnel_grid[:, 0])
            tunnel_grid = tunnel_grid[tunnel_grid[:, 1] >= y_func - tunnel_span]

            grids = tunnel_grid if grids is None else np.concatenate((grids, tunnel_grid))

        return grids

    def _make_regular_grid(self, x_min: float, x_max: float, y_min: float, y_max: float):
        """ Make a points grid based on the regular strategy.

        Parameters
        ----------
        x_min : float
            Starting x of a grid.

        x_max : float
            Ending x of a grid.

        y_min: float
            Starting y of a grid.

        y_max: float
            Ending y of a grid.

        Returns
        -------
        grid : np.ndarray
            Made regular points grid.
        """
        xs = np.arange(x_min, x_max, 0.1)
        ys = np.arange(y_min, y_max, 0.1)

        grid = []
        for x in xs:
            for y in ys:
                grid.append([x, y])

        return np.array(grid)

    @property
    def approx_funcs(self):
        return self._approx_funcs

    def _validate_state(self):
        if not isinstance(self._approx_funcs, PolynomialFunction) and not isinstance(self._approx_funcs, Iterable):
            raise TypeError(f'Invalid type of approx_funcs. Could not be {type(self._approx_funcs)}')

        if not isinstance(self._n_points, int):
            raise TypeError(f'Invalid type of n_points. Could not be {type(self._n_points)}')

        if not isinstance(self._grid_strategy, str):
            raise TypeError(f'Invalid type of grid_strategy. Could not be {type(self._grid_strategy)}')
