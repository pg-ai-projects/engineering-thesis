import pytest
import tensorflow as tf
import numpy as np


@pytest.fixture(scope='session', autouse=True)
def test_data():
    pytest.sigmoid_test_model = tf.keras.models.Sequential([
        tf.keras.layers.Dense(1, activation='sigmoid')
    ])
    pytest.softmax_test_model = tf.keras.models.Sequential([
        tf.keras.layers.Dense(2, activation='softmax')
    ])
    pytest.relu_test_model = tf.keras.models.Sequential([
        tf.keras.layers.Dense(1, activation='relu')
    ])
    pytest.x = np.array([[0, 0]])
    pytest.y = np.array([0])
    pytest.multi_x = np.array([[0, 0], [1, 1], [2, 2]])
    pytest.multi_y = np.array([0, 0, 1])


@pytest.fixture(scope='session')
def complex_test_data():
    pytest.extraction_precision = 0.2

    pytest.two_dim_points_array = np.array([
        [.0, .0], [.2, .0], [.4, .0],
        [.0, .2], [.2, .2], [.4, .2],
        [.0, .4], [.2, .4], [.4, .4],
    ])
    pytest.two_dim_array_pred_labels = np.array([0, 1, 1, 0, 0, 1, 0, 0, 0])
    pytest.two_dim_array_model_pred_labels = np.array([[0], [1], [1], [0], [0], [1], [0], [0], [0]])
    pytest.two_dim_array_extracted_points = np.array([[0.2, 0.1], [0.4, 0.3], [0.1, 0.], [0.3, 0.2]])

    pytest.three_dim_points_array = np.array([
        [.0, .0, .0], [.2, .0, .0],
        [.0, .2, .0], [.2, .2, .0],

        [.0, .0, .2], [.2, .0, .2],
        [.0, .2, .2], [.2, .2, .2],
    ])
    pytest.three_dim_array_pred_labels = np.array([0, 1, 1, 1, 1, 1, 1, 1])
    pytest.three_dim_array_model_pred_labels = np.array([[0], [1], [1], [1], [1], [1], [1], [1]])
    pytest.three_dim_array_extracted_points = np.array([[.0, .1, .0], [.1, .0, .0], [.0, .0, .1]])

