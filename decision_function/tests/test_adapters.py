from unittest import mock

import numpy as np
import pytest

from decision_function.adapters import SigmoidAIModel, SoftmaxAIModel


# ==================================
#       SigmoidAIModel tests

def test_SigmoidAIModel_with_model_not_being_Sequential():
    model = object()
    with pytest.raises(TypeError):
        SigmoidAIModel(model)


def test_SigmoidAIModel_with_model_being_none():
    model = None
    with pytest.raises(TypeError):
        SigmoidAIModel(model)


def test_SigmoidAIModel_with_last_layer_not_being_sigmoid():
    model = pytest.softmax_test_model
    with pytest.raises(TypeError):
        SigmoidAIModel(model)


def test_SigmoidAIModel_with_none_labels():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=np.array([[0.75]]))
    adapter = SigmoidAIModel(model)

    label = adapter.predict(pytest.x)

    assert label == 1


def test_SigmoidAIModel_with_defined_labels():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=np.array([[0.99]]))
    labels = ['one', 'two']
    adapter = SigmoidAIModel(model, labels)

    label = adapter.predict(pytest.x)

    assert label == 'two'


def test_SigmoidAIModel_with_multi_x_with_none_labels():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=np.array([[0.67], [0.2], [0.9]]))
    adapter = SigmoidAIModel(model)

    labels = adapter.predict(pytest.multi_x)

    assert all(labels == [1, 0, 1])
    model.predict.assert_called_once()


def test_SigmoidAIModel_with_multi_x_with_defined_labels():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=np.array([[0.55], [0.40], [0.25]]))
    adapter = SigmoidAIModel(model, ['one', 'two'])

    labels = adapter.predict(pytest.multi_x)

    assert all(labels == ['two', 'one', 'one'])
    model.predict.assert_called_once()


def test_SigmoidAIModel_with_probability_equal_50():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=np.array([[0.50]]))
    adapter = SigmoidAIModel(model)

    label = adapter.predict(pytest.x)

    assert label in [0, 1]


def test_SigmoidAIModel_fit_to_class_with_prob_not_being_ndarray():
    model = pytest.sigmoid_test_model
    adapter = SigmoidAIModel(model)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(10)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(0.25)


def test_SigmoidAIModel_fit_to_class_with_prob_being_none():
    model = pytest.sigmoid_test_model
    adapter = SigmoidAIModel(model)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(None)


def test_SigmoidAIModel_fit_to_class_with_proper_prob():
    model = pytest.sigmoid_test_model
    adapter = SigmoidAIModel(model)

    label = adapter.fit_to_class(np.array([0.2]))

    assert label == 0


def test_SigmoidAIModel_with_multi_probabilities():
    model = pytest.sigmoid_test_model
    adapter = SigmoidAIModel(model)

    labels = adapter.fit_to_class(np.array([[0.25], [0.58]]))

    assert all(labels == [0, 1])


# ==================================
#       SoftmaxAIModel tests

def test_SoftmaxAIModel_with_model_not_being_Sequential():
    model = object()
    with pytest.raises(TypeError):
        SoftmaxAIModel(model)


def test_SoftmaxAIModel_with_model_being_none():
    model = None
    with pytest.raises(TypeError):
        SoftmaxAIModel(model)


def test_SoftmaxAIModel_with_last_layer_not_being_softmax():
    model = pytest.sigmoid_test_model
    with pytest.raises(TypeError):
        SoftmaxAIModel(model)


def test_SoftmaxAIModel_with_none_labels():
    model = pytest.softmax_test_model
    model.predict = mock.Mock(return_value=np.array([[0.77, 0.23]]))
    adapter = SoftmaxAIModel(model)

    label = adapter.predict(pytest.x)

    assert label == 0


def test_SoftmaxAIModel_with_defined_labels():
    model = pytest.softmax_test_model
    model.predict = mock.Mock(return_value=np.array([[0.13, 0.87]]))
    labels = ['one', 'two']
    adapter = SoftmaxAIModel(model, labels)

    label = adapter.predict(pytest.x)

    assert label == 'two'


def test_SoftmaxAIModel_with_multi_x_with_none_labels():
    model = pytest.softmax_test_model
    model.predict = mock.Mock(return_value=np.array([[0.75, 0.25], [0.7, 0.3], [0.15, 0.85]]))
    adapter = SoftmaxAIModel(model)

    labels = adapter.predict(pytest.multi_x)

    assert all(labels == [0, 0, 1])
    model.predict.assert_called_once()


def test_SoftmaxAIModel_with_multi_x_with_defined_labels():
    model = pytest.softmax_test_model
    model.predict = mock.Mock(return_value=np.array([[0.65, 0.35], [0.51, 0.49], [0.13, 0.87]]))
    adapter = SoftmaxAIModel(model, ['one', 'two'])

    labels = adapter.predict(pytest.multi_x)

    assert all(labels == ['one', 'one', 'two'])
    model.predict.assert_called_once()


def test_SoftmaxAIModel_with_probability_equal_50():
    model = pytest.softmax_test_model
    model.predict = mock.Mock(return_value=np.array([[0.50, 0.50]]))
    adapter = SoftmaxAIModel(model)

    label = adapter.predict(pytest.x)

    assert label in [0, 1]


def test_SoftmaxAIModel_fit_to_class_with_prob_not_being_ndarray():
    model = pytest.softmax_test_model
    adapter = SoftmaxAIModel(model)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(10)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(0.25)


def test_SoftmaxAIModel_fit_to_class_with_prob_being_none():
    model = pytest.softmax_test_model
    adapter = SoftmaxAIModel(model)

    with pytest.raises(TypeError):
        label = adapter.fit_to_class(None)


def test_SoftmaxAIModel_fit_to_class_with_proper_prob():
    model = pytest.softmax_test_model
    adapter = SoftmaxAIModel(model)

    label = adapter.fit_to_class(np.array([0.3, 0.7]))

    assert label == 1


def test_SoftmaxAIModel_with_multi_probabilities():
    model = pytest.softmax_test_model
    adapter = SoftmaxAIModel(model)

    labels = adapter.fit_to_class(np.array([[0.32, 0.68], [0.55, 0.45]]))

    assert all(labels == [1, 0])
