import unittest
import numpy as np

from decision_function.approximation import FunctionApproximator
from decision_function.adapters import PolynomialFunction


class FunctionApproximatorTests(unittest.TestCase):

    def test_degree_lower_than_0_raises_exception(self):
        with self.assertRaises(ValueError):
            FunctionApproximator(degree=-1)

    def test_degree_not_being_int_raises_exception(self):
        with self.assertRaises(TypeError):
            FunctionApproximator(degree="1")

    def test_degree_being_none_raises_exception(self):
        with self.assertRaises(TypeError):
            FunctionApproximator(degree=None)

    def test_alpha_not_being_float_or_int_raises_exception(self):
        with self.assertRaises(TypeError):
            FunctionApproximator(degree=1, alpha="1")

    def test_alpha_being_none_raises_exception(self):
        with self.assertRaises(TypeError):
            FunctionApproximator(degree=1, alpha=None)

    def test_not_fitted_approximation_raises_exception(self):
        model = FunctionApproximator(degree=1)

        with self.assertRaises(PermissionError):
            model.approximate()

    def test_fitted_changed_after_fit(self):
        model = FunctionApproximator(degree=1)

        model.fit(np.array([[1], [2], [3], [4], [5]]))

        self.assertTrue(model._fitted)

    def test_point_class_not_being_None_or_int(self):
        with self.assertRaises(TypeError):
            model = FunctionApproximator(degree=1, points_class='s')

    def test_inequalities_not_being_None_or_dict(self):
        with self.assertRaises(TypeError):
            model = FunctionApproximator(degree=1, points_class=1, inequalities='s')


class FunctionApproximatorPredictionTests(unittest.TestCase):

    def setUp(self) -> None:
        self.fitted_2d_model = FunctionApproximator(degree=1).fit(np.array([[1, 1], [2, 2]]))

    # ======================================================

    def test_approximation_returns_PolynomialFunction(self):
        model = self.fitted_2d_model
        
        function = model.approximate()
        
        self.assertIsInstance(function, PolynomialFunction)

    def test_predict_with_data_being_None(self):
        model = self.fitted_2d_model
        function = model.approximate()

        with self.assertRaises(TypeError):
            function.predict(None)

    def test_predict_with_data_being_tuple(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function.predict(((1,), (2,)))

    def test_predict_with_data_being_list(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function.predict([[1], [2]])

    def test_predict_with_data_being_string(self):
        model = self.fitted_2d_model
        function = model.approximate()

        with self.assertRaises(TypeError):
            function.predict("numbers")

    def test_predict_with_1D_list_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function.predict([1, 0])

    def test_predict_with_1D_tuple_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function.predict((1, 0))

    def test_predict_with_1D_ndarray_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function.predict(np.array([1, 0]))

    def test_predict_with_data_shape_being_different_than_fitting(self):
        model = self.fitted_2d_model # 2 DIM
        function = model.approximate()

        with self.assertRaises(TypeError):
            function.predict(np.array([[1], [0]])[:, np.newaxis]) # 3 DIM

    def test_call_with_data_being_None(self):
        model = self.fitted_2d_model
        function = model.approximate()

        with self.assertRaises(TypeError):
            function(None)

    def test_call_with_data_being_tuple(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function(((1,), (2,)))

    def test_call_with_data_being_list(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function([[1], [2]])

    def test_call_with_data_being_string(self):
        model = self.fitted_2d_model
        function = model.approximate()

        with self.assertRaises(TypeError):
            function("numbers")

    def test_call_with_data_element_not_being_int_or_float(self):
        model = self.fitted_2d_model
        function = model.approximate()

        with self.assertRaises(TypeError):
            function([[1], [1.0], ['1']])

    def test_call_with_1D_list_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function([1, 0])

    def test_call_with_1D_tuple_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function((1, 0))

    def test_call_with_1D_ndarray_data(self):
        model = self.fitted_2d_model
        function = model.approximate()
        function(np.array([1, 0]))

    def test_call_with_data_shape_being_different_than_fitting(self):
        model = self.fitted_2d_model  # 2 DIM
        function = model.approximate()

        with self.assertRaises(TypeError):
            function(np.array([[1], [0]])[:, np.newaxis])  # 3 DIM


if __name__ == '__main__':
    unittest.main()
