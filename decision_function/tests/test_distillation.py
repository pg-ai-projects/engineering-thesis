import unittest

from decision_function.distillation import KnowledgeDistillator
from decision_function.generation import GridGenerator


class KnowledgeDistillatorTest(unittest.TestCase):

    def test_dense_units_and_activations_lengths_are_different_raises_exception(self):
        with self.assertRaises(ValueError):
            KnowledgeDistillator([8, 16, 2], ['relu', 'softmax'], 100, GridGenerator([], 500, 'regular'))

    def test_one_element_in_dense_units_not_being_int_raises_exception(self):
        with self.assertRaises(TypeError):
            KnowledgeDistillator(['8', 16, 2], ['relu', 'relu', 'softmax'], 100, GridGenerator([], 500, 'regular'))

    def test_all_elements_in_activations_not_being_str_raises_exception(self):
        with self.assertRaises(TypeError):
            KnowledgeDistillator([8, 16, 2], [8, 8, 8], 100, GridGenerator([], 500, 'regular'))

    def test_last_element_in_activations_not_being_softmax_or_sigmoid_raises_exception(self):
        with self.assertRaises(ValueError):
            KnowledgeDistillator([8, 16, 2], ['relu', 'relu', 'relu'], 100, GridGenerator([], 500, 'regular'))

    def test_epochs_not_being_int_raises_exception(self):
        with self.assertRaises(TypeError):
            KnowledgeDistillator([8, 16, 2], ['relu', 'relu', 'softmax'], '100', GridGenerator([], 500, 'regular'))

    def test_grid_generator_not_being_GridGenerator_raises_exception(self):
        with self.assertRaises(TypeError):
            KnowledgeDistillator([8, 16, 2], ['relu', 'relu', 'softmax'], 100, 'str')


if __name__ == '__main__':
    unittest.main()
