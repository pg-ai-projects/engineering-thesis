import warnings

import pytest
from unittest import mock
import numpy as np

from decision_function.extraction import DecisionFunctionExtractor, ExtractedPoints


def test_init_DecisionFunctionExtractor_with_model_not_being_softmax_or_sigmoid():
    model = pytest.relu_test_model

    with pytest.raises(NotImplementedError):
        DecisionFunctionExtractor(model)


def test_init_DecisionFunctionExtractor_with_model_being_none():
    with pytest.raises(TypeError):
        DecisionFunctionExtractor(None)


def test_init_DecisionFunctionExtractor_with_model_not_being_Sequential():
    with pytest.raises(TypeError):
        DecisionFunctionExtractor(1)


def test_init_DecisionFunctionExtractor_with_precision_not_being_float():
    model = pytest.softmax_test_model

    with pytest.raises(TypeError):
        DecisionFunctionExtractor(model, 10)


def test_DecisionFunctionExtractor_execute_extract_without_fitting():
    model = pytest.softmax_test_model

    with pytest.raises(PermissionError):
        DecisionFunctionExtractor(model).extract()


def test_DecisionFunctionExtractor_execute_fit_with_x_not_being_ndarray():
    model = pytest.softmax_test_model

    with pytest.raises(TypeError):
        DecisionFunctionExtractor(model).fit(10, pytest.y)


def test_DecisionFunctionExtractor_execute_fit_with_x_being_none():
    model = pytest.softmax_test_model

    with pytest.raises(TypeError):
        DecisionFunctionExtractor(model).fit(None, pytest.y)


def test_DecisionFunctionExtractor_execute_fit_with_x_with_unallowed_shape():
    model = pytest.softmax_test_model
    x = np.array([[[0, 1]]])

    with pytest.raises(ValueError):
        DecisionFunctionExtractor(model).fit(x, pytest.y)


def test_DecisionFunctionExtractor_execute_fit_with_y_not_being_ndarray():
    model = pytest.softmax_test_model

    with pytest.raises(TypeError):
        DecisionFunctionExtractor(model).fit(pytest.x, 1)


def test_DecisionFunctionExtractor_execute_fit_with_y_being_none():
    model = pytest.softmax_test_model

    with pytest.raises(TypeError):
        DecisionFunctionExtractor(model).fit(pytest.x, None)


def test_DecisionFunctionExtractor_execute_fit_with_y_with_unallowed_shape():
    model = pytest.softmax_test_model
    y = np.array([[1], [2], [1]])

    with pytest.raises(ValueError):
        DecisionFunctionExtractor(model).fit(pytest.multi_x, y)


def test_DecisionFunctionExtractor_execute_fit_correctly_change_fitted():
    model = pytest.softmax_test_model
    extractor = DecisionFunctionExtractor(model)

    assert extractor._fitted is False
    extractor.fit(pytest.x, pytest.y)
    assert extractor._fitted is True


def test_DecisionFunctionExtractor_extraction_with_2d_ndarray(complex_test_data):
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=pytest.two_dim_array_model_pred_labels)
    extractor = DecisionFunctionExtractor(model, precision=pytest.extraction_precision)
    extractor.fit(pytest.two_dim_points_array, pytest.two_dim_array_pred_labels)

    points = extractor.extract(max_offset=0, min_offset=0).classes_points[0].astype(np.float32)

    assert (points == pytest.two_dim_array_extracted_points.astype(np.float32)).all()


def test_DecisionFunctionExtractor_extraction_with_3d_ndarray(complex_test_data):
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=pytest.three_dim_array_model_pred_labels)
    extractor = DecisionFunctionExtractor(model, precision=pytest.extraction_precision)
    extractor.fit(pytest.three_dim_points_array, pytest.three_dim_array_pred_labels)

    points = extractor.extract(max_offset=0, min_offset=0).classes_points[0]
    points = points.astype(np.float32)

    assert (points == pytest.three_dim_array_extracted_points.astype(np.float32)).all()


def test_DecisionFunctionExtractor_extraction_without_data_standardization():
    model = pytest.softmax_test_model
    extractor = DecisionFunctionExtractor(model)

    with warnings.catch_warnings():
        extractor.fit(pytest.multi_x, pytest.multi_y)
        warnings.simplefilter('once')


def test_DecisionFunctionExtractor_returns_ExtractedPoints_object():
    model = pytest.sigmoid_test_model
    model.predict = mock.Mock(return_value=pytest.three_dim_array_model_pred_labels)
    extractor = DecisionFunctionExtractor(model, precision=pytest.extraction_precision)
    extractor.fit(pytest.three_dim_points_array, pytest.three_dim_array_pred_labels)

    extraction = extractor.extract(max_offset=0, min_offset=0)

    assert isinstance(extraction, ExtractedPoints)
