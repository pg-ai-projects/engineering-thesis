import unittest
import numpy as np

from decision_function.generation import GridGenerator


class GridGeneratorTest(unittest.TestCase):

    def test_approx_funcs_not_being_PolynomialFunction_or_Iterable_raises_exception(self):
        with self.assertRaises(TypeError):
            GridGenerator(5, 500, 'random')

    def test_n_points_not_being_int_raises_exception(self):
        with self.assertRaises(TypeError):
            GridGenerator([], '500', 'random')

    def test_grid_strategy_not_being_str_raises_exception(self):
        with self.assertRaises(TypeError):
            GridGenerator([], 500, 5)

    def test_make_regular_grid(self):
        grid = GridGenerator([], 500, 'regular').make_grid(1, 1.2, 1, 1.2)
        assert (grid == np.array([[1, 1], [1, 1.1], [1.1, 1], [1.1, 1.1]])).all()


if __name__ == '__main__':
    unittest.main()
