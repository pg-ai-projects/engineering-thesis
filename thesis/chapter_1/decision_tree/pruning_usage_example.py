import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import time

from sklearn import datasets
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split, GridSearchCV
from sklearn.preprocessing import StandardScaler
from sklearn.tree import DecisionTreeClassifier


def load_diabetes_dataset(as_dataframe=True):
    ds = datasets.load_breast_cancer()

    if not as_dataframe:
        return ds

    print(ds.DESCR)

    columns = list(ds['feature_names'])
    columns.extend(['target'])

    # np.c_ is the numpy concatenate function
    df = pd.DataFrame(data=np.c_[ds['data'], ds['target']],
                      columns=columns)
    return df


def prepare_train_test_datasets(df: pd.DataFrame):
    y = df['target'].values
    x = df.drop('target', axis=1).to_numpy()

    sc = StandardScaler()
    x_std = sc.fit_transform(x)

    x_train, y_train, x_test, y_test = train_test_split(x_std, y, train_size=0.5, random_state=1)

    return x_train, x_test, y_train, y_test


def create_tree_classifier(*cls_args, **cls_kwargs):
    tree_cls = DecisionTreeClassifier(*cls_args, **cls_kwargs)
    return tree_cls


def plot_features_importances(importances: list, bars_ticks: list[str]):
    features_count = len(importances)
    x = np.arange(features_count)
    steps_values = [0 for i in range(features_count)]

    features = zip(importances, bars_ticks)
    features = sorted(features, key=lambda f: f[0], reverse=True)
    importances, bars_ticks = zip(*features)

    for i, importance in enumerate(importances):
        steps_values[i] = importance + steps_values[i - 1]

    fig = plt.figure(figsize=(16, 5))

    plt.bar(x, importances, align='center', label="Wartość informatywna atrybutu")
    plt.step(x, steps_values, c="black", where="mid", label="Całkowita wartość krocząca informatywności atrybutów")
    plt.xticks(range(len(bars_ticks)), labels=bars_ticks, rotation=60, fontsize=15)

    plt.title('Informatywność poszczególnych atrybutów zbioru danych "Breast cancer wisconsin"', fontsize=15)
    plt.ylabel("Względna wartość informatywna", fontsize=15)
    plt.legend(loc='best')
    plt.tight_layout()
    plt.show()


def main():
    from sklearn import tree

    df = load_diabetes_dataset()
    x_train, y_train, x_test, y_test = prepare_train_test_datasets(df)

    tree_cls = create_tree_classifier()
    tree_cls.fit(x_train, y_train)
    tree.plot_tree(tree_cls)
    
    y_pred = tree_cls.predict(x_test)
    
    print(f'Accuracy scored: {accuracy_score(y_test, y_pred)}')

    tree_cls = create_tree_classifier(max_depth=4)
    tree_cls.fit(x_train, y_train)

    fi = tree_cls.feature_importances_
    plot_features_importances(fi, df.columns.values.astype(np.str))
    print(df.head(20))
    plt.figure(figsize=(16, 12))
    tree.plot_tree(tree_cls,
                   feature_names=df.columns.values.astype(np.str),
                   class_names=("rak łagodny", "rak złośliwy"),
                   filled=True,
                   rounded=True)
    plt.show()
    y_pred = tree_cls.predict(x_test)

    print(f'Accuracy scored: {accuracy_score(y_test, y_pred)}')

    param_grid = {
        "max_depth": range(1, 30)
    }
    gs = GridSearchCV(estimator=DecisionTreeClassifier(random_state=1),
                      param_grid=param_grid,
                      scoring='accuracy')
    gs.fit(x_train, y_train)
    print(f"The best combination: {gs.best_params_}, score: {gs.best_score_}")


if __name__ == '__main__':
    main()
