import numpy as np
import matplotlib.pyplot as plt
import pandas as pd

from sklearn.linear_model import LinearRegression, Ridge, Lasso, LogisticRegression
from sklearn.metrics import mean_squared_error, r2_score


LEGEND_FONTSIZE = 12
TITLE_FONTSIZE = 12


def create_points_dataset():
    ds = pd.DataFrame(data={
        'x': [1,   7,   6,   1.25, 2, 3.5, 5,   1.8, 2.4, 4,   6.5,  3.2, 4.5, 5.5, 3,   1.2, 1.5, 3, 3.5, 4, 4.5, 5.75, 6,   7,    2,   1.8, 5.5, 2.5, 5, 6.8],
        'y': [2.8, 1.4, 0.8, 1.5,  1, 2.4, 1.5, 2.5, 2,   1.5, 1.25, 1.8, 1,   1.8, 1.2, 3.5, 4.5, 3, 3.2, 4, 3,   3.05, 2.2, 1.9,  3.2, 3.2, 3.5, 4,   2.4, 2.3],
        'c': ['#d40e00' for _ in range(15)] + ['#3258a8' for _ in range(15)],
        'size': 15
    })
    return ds


def main():
    ds = create_points_dataset()
    print(ds.head())

    # =====================
    # Dataset visualization
    plt.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], marker='x', label='Klasa A')
    plt.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:], label='Klasa B')
    plt.title('Stworzony zbiór danych', fontsize=TITLE_FONTSIZE)
    plt.xticks([])
    plt.yticks([])
    plt.legend(fontsize=LEGEND_FONTSIZE)
    plt.tight_layout()
    plt.show()

    # ===========================================
    # Linear regression without L2 regularization
    X_native, y_native = list(map(lambda x: [x], ds['x'])), list(map(lambda y: [y], ds['y']))
    X_test = [[i] for i in np.linspace(1, 7, 100)]

    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10, 5))

    lr = LinearRegression()
    lr.fit(X_native, y_native)

    ax0.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], marker='x')
    ax0.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:])
    ax0.plot(X_test, lr.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax0.set_title('Regresja liniowa na pełnym zbiorze danych', fontsize=TITLE_FONTSIZE)
    # ax0.set_xticks([])
    # ax0.set_yticks([])
    ax0.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y_native, lr.predict(X_native)))
    print(f'Coefficients: {lr.coef_}, {r2_score(y_native, lr.predict(X_native))}')

    X, y = ds['x'][:3].append(ds['x'][-3:]), ds['y'][:3].append(ds['y'][-3:])
    X, y = list(map(lambda x: [x], X)), list(map(lambda y: [y], y))
    lr.fit(X, y)

    ax1.scatter(X[:3], y[:3], c=ds['c'][:3], marker='x')
    ax1.scatter(X[-3:], y[-3:], c=ds['c'][-3:])
    ax1.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], alpha=0.15, marker='x')
    ax1.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:], alpha=0.15)
    ax1.plot(X_test, lr.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax1.set_title('Regresja liniowa na zredukowanym zbiorze danych', fontsize=TITLE_FONTSIZE)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y_native, lr.predict(X_native)))

    plt.tight_layout()
    plt.show()

    # ===========================================
    # Linear regression without L2 regularization
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10, 5))

    ax0.scatter(X[:3], y[:3], c=ds['c'][:3], marker='x')
    ax0.scatter(X[-3:], y[-3:], c=ds['c'][-3:])
    ax0.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], alpha=0.15, marker='x')
    ax0.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:], alpha=0.15)
    ax0.plot(X_test, lr.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax0.set_title('Regresja liniowa na zredukowanym zbiorze danych', fontsize=TITLE_FONTSIZE)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y, lr.predict(X)))
    print(f'Coefs: {lr.coef_}, {lr.intercept_}')

    rc = Ridge(alpha=10).fit(X, y)
    ax1.scatter(X[:3], y[:3], c=ds['c'][:3], marker='x')
    ax1.scatter(X[-3:], y[-3:], c=ds['c'][-3:])
    ax1.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], alpha=0.15, marker='x')
    ax1.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:], alpha=0.15)
    ax1.plot(X_test, rc.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax1.set_title('Regresja liniowa z użyciem regularyzacji L2, gamma = 10', fontsize=TITLE_FONTSIZE)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y_native, rc.predict(X_native)))
    print(f'Coefs: {rc.coef_}, {rc.intercept_}')

    plt.tight_layout()
    plt.show()

    # ====================================================================
    # Comparison of different values of L2 regularization gamma parameter
    fig = plt.figure(figsize=(8, 6))
    alphas = [0.1, 1, 10, 100, 500, 1000]
    lines_styles = ['-', '--', '-.', ':', '--', '-.']

    for alpha, line_style in zip(alphas, lines_styles):
        rc = Ridge(alpha=alpha).fit(X, y)
        plt.plot(X_test, rc.predict(X_test), line_style, label=f'gamma = {alpha}', )

    plt.scatter(X[:3], y[:3], c=ds['c'][:3], marker='x')
    plt.scatter(X[-3:], y[-3:], c=ds['c'][-3:])
    plt.title('Regresja liniowa z użyciem regularyzacji L2 o róznych wartościach parametru gamma', fontsize=TITLE_FONTSIZE)
    plt.xticks([])
    plt.yticks([])
    plt.legend(fontsize=LEGEND_FONTSIZE)
    plt.tight_layout()
    plt.show()

    # ===================================================================================================
    # Linear regression with L2 regression compared to linear regression without them but on full dataset
    fig, (ax0, ax1) = plt.subplots(1, 2, figsize=(10, 5))

    lr = LinearRegression().fit(X_native, y_native)
    ax0.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], marker='x')
    ax0.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:])
    ax0.plot(X_test, lr.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax0.set_title('Regresja liniowa na pełnym zbiorze danych', fontsize=TITLE_FONTSIZE)
    ax0.set_xticks([])
    ax0.set_yticks([])
    ax0.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y_native, lr.predict(X_native)))

    rc = Ridge(alpha=30).fit(X, y)
    ax1.scatter(X[:3], y[:3], c=ds['c'][:3], marker='x')
    ax1.scatter(X[-3:], y[-3:], c=ds['c'][-3:])
    ax1.scatter(ds['x'][:15], ds['y'][:15], c=ds['c'][:15], alpha=0.15, marker='x')
    ax1.scatter(ds['x'][15:], ds['y'][15:], c=ds['c'][15:], alpha=0.15)
    ax1.plot(X_test, rc.predict(X_test), label='Funkcja decyzyjna regresji liniowej', c='#00d115')
    ax1.set_title('Regresja liniowa z użyciem regularyzacji L2, gamma = 30', fontsize=TITLE_FONTSIZE)
    ax1.set_xticks([])
    ax1.set_yticks([])
    ax1.legend(fontsize=LEGEND_FONTSIZE)
    print("Mean squared error: %.3f" % mean_squared_error(y_native, rc.predict(X_native)))

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
