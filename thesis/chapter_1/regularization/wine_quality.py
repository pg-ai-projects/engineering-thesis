import tensorflow as tf
import tensorflow_datasets as tfds

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

import sklearn.model_selection
from sklearn.metrics import accuracy_score


def load_wine_quality_dataset(wine_type: str = "white") -> (pd.DataFrame, tfds.core.DatasetInfo):
    """ Load wine_quality dataset.

    Parameters
    ----------
    wine_type : str. Optional. Default = 'white'
        Type of needed wine dataset. Could be 'white' or 'red'.

    Returns
    -------
        (df, ds_info) : (pd.DataFrame, tf.DatasetInfo)
            Loaded DataFrame with downloaded wine quality data and info about loaded dataset.

    Raises
    ------
        AttributeError
            When input win_type argument is different type than allowed.
            When input wine_type argument value is different that allowed.

    Notes
    -----
        Function uses tensorflow_datasets methods to get exactly version defined by user of wine quality dataset.
        tensorflow_datasets.load() and then tensorflow_datasets.as dataframe() are used for creating returned objects.
    """
    ALLOWED_WINE_TYPES_VALUES = ["red", "white"]

    if not isinstance(wine_type, str):
        raise AttributeError("Illegal wine_type attribute type. Should be {allowed_types}, not '{attr_type}'".format(
            allowed_types=str,
            attr_type=type(wine_type)
        ))

    if not (wine_type.lower() in ALLOWED_WINE_TYPES_VALUES):
        raise AttributeError("Illegal attribute value. "
                             "Allowed values: {allowed_values}, not {attr_value}".format(
            allowed_values=ALLOWED_WINE_TYPES_VALUES,
            attr_value=wine_type
        ))

    dataset = 'wine_quality'
    if wine_type.lower() == "white":
        dataset += "/white"
    elif wine_type.lower() == "red":
        dataset += "/red"

    dataset, ds_info = tfds.load(
        dataset,
        with_info=True,
    )

    ds_train = dataset["train"]
    df = tfds.as_dataframe(ds_train)
    df = pd.DataFrame(df)

    dataset, ds_info = tfds.load(
        'wine_quality/red',
        with_info=True,
    )
    ds_train = dataset["train"]
    df2 = tfds.as_dataframe(ds_train)
    df2 = pd.DataFrame(df2)
    df = pd.concat([df, df2], ignore_index=True)

    return df, ds_info


def process_dataset(df: pd.DataFrame) -> pd.DataFrame:
    """ Process provided dataframe to prepare data uses in AI model.

    Parameters
    ----------
    df : pd.DataFrame
        DataFrame with data to process.

    Returns
    -------
    df : pd.DataFrame
        Proceed DataFrame.

    Notes
    -----
    Function does following things:
    1. Removes rows with empty values.
    2. Removes 'feature/' string from every column name (if occur).
    3. Changes 'quality' column values from 3 - 9 to 0 - 6.
    4. Removes unneeded rows.
    """

    # remove rows from dataframe that have empty value in any column
    empty_rows, empty_cols = np.where(pd.isnull(df))
    if len(empty_rows) > 0:
        print("{no_empty_rows} rows are dropped due to empty values".format(
            no_empty_rows=len(np.unique(empty_rows))))
        df.dropna(inplace=True)
        df.reset_index(inplace=True)

    # remove 'features/' element from each column name
    df.columns = list(
        map(lambda col: col[len("features/"):] if "features/" in col else col, df.columns)
    )

    # map quality values [3, 4, 5, 6, 7, 8, 9] to [0, 1, 2, 3, 4, 5, 6]
    df['quality'] = df['quality'].map(lambda q: q-3)

    # make data standardized
    for column in df.columns:
        if column != "quality":
            df[column] = (df[column] - df[column].mean()) / df[column].std()

    df = df.iloc[df['quality'].values != 0, :]
    df = df.iloc[df['quality'].values != 1, :]
    df = df.iloc[df['quality'].values != 5, :]
    df = df.iloc[df['quality'].values != 6, :]
    df['quality'] = df['quality'].map(lambda q: q - 2)

    return df


def split_train_valid_test(df: pd.DataFrame, valid_size: float = 0.20, test_size: float = 0.15) -> (np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray):
    """ Split provided DataFrame to sets for training, validating and testing.

    Parameters
    ----------
    df : pd.DataFrame
        Dataframe with values to split.

    valid_size : float Optional. Default = 0.2.
        Percentage size of validation set. Must be between 0.0 and 1.0.

    test_size : float Optional. Default = 0.15.
        Percentage size of testing set. Must be between 0.0 and 1.0.

    Returns
    -------
    (x_train, x_valid, x_test, y_train, y_valid, y_test) : (np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray, np.ndarray)
        Split sets.
    """
    y = df['quality'].values
    df.drop("quality", axis=1, inplace=True)

    x = df.to_numpy()
    x_train, x_test, y_train, y_test = sklearn.model_selection.train_test_split(x, y,
                                                                                test_size=test_size,
                                                                                shuffle=True, stratify=y)

    if valid_size > 0.0:
        x_train, x_valid, y_train, y_valid = sklearn.model_selection.train_test_split(x_train, y_train,
                                                                                      test_size=(valid_size/(1-test_size)),
                                                                                      shuffle=True, stratify=y_train)
    else:
        x_valid, y_valid = [], []

    return x_train, x_valid, x_test, y_train, y_valid, y_test


def create_model() -> tf.keras.models.Sequential:
    """ Create AI model solving wine quality set problem. """
    model = tf.keras.models.Sequential([
        tf.keras.layers.Dense(units=32, activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.008)),
        tf.keras.layers.Dense(units=64, activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.008)),
        tf.keras.layers.Dense(units=32, activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.008)),
        tf.keras.layers.Dense(units=16, activation='relu', kernel_regularizer=tf.keras.regularizers.L2(0.008)),
        tf.keras.layers.Dense(units=7, activation='softmax')
    ])
    model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
    return model


def evaluate_model(history: tf.keras.callbacks.History) -> None:
    """ Print history of model training.

    Parameters
    ----------
    history : tf.keras.callbacks.History
        Training model history object.
    """
    df_hist = pd.DataFrame(history.history)

    fig, axs = plt.subplots(1, 2)
    fig.set_figheight(9)
    fig.set_figwidth(16)

    axs[0].plot(df_hist["loss"], label="zbiór uczący")
    axs[0].plot(df_hist["val_loss"], label="zbiór walidacyjny")
    axs[0].set_title('Wartość funkcji kosztu podczas uczenia modelu')
    axs[0].set_xlabel('epoka')
    axs[0].set_ylabel('wartość')
    axs[0].legend()

    axs[1].plot(df_hist["accuracy"], label='zbiór uczący')
    axs[1].plot(df_hist["val_accuracy"], label='zbiór walidacyjny')
    axs[1].set_title('Skuteczności modelu podczas uczenia')
    axs[1].set_xlabel('epoka')
    axs[1].set_ylabel('wartość')
    axs[1].legend()

    plt.tight_layout()
    plt.show()


def main():
    # 1. Download and load wine quality dataset.
    df, ds_info = load_wine_quality_dataset()
    print(ds_info)

    # 2. Prepare dataset for using data in machine learning.
    df = process_dataset(df)

    # 3. Split dataset to train, test and validation sets.
    x_train, x_valid, x_test, y_train, y_valid, y_test = split_train_valid_test(df, valid_size=0.15, test_size=0.25)

    # 4. Prepare, compile and train DNN model.
    model = create_model()

    history = model.fit(
        x_train, y_train,
        validation_data=(x_valid, y_valid),
        epochs=80
    )
    y_pred = model.predict(x_test)
    y_pred = list(
        map(lambda y: y.tolist().index(max(y)), y_pred)
    )
    print(np.unique(y_pred))
    print(f"Accuracy achieved: {accuracy_score(y_test, y_pred)}%")

    # 5. Evaluate created model.
    evaluate_model(history)


if __name__ == "__main__":
    main()
