import matplotlib.pyplot as plt
import numpy as np

from sklearn.linear_model import Ridge
from sklearn.preprocessing import PolynomialFeatures, SplineTransformer
from sklearn.pipeline import make_pipeline

from scipy.interpolate import interp1d


def func(x: np.ndarray):
    return (x-2)**3 + 8


def func2(x):
    return np.sin(x) * np.cos(x) * x


def main():
    x = np.arange(0, 5, 0.01)
    y = func2(x)
    
    ds = np.random.rand(30)
    ds = ds * (np.max(x) - np.min(x)) - np.min(x)
    ds = np.array(list(zip(ds, func2(ds))))
    
    plt.plot(x, y, label='Funkcja rzeczywista')
    plt.scatter(ds[:, 0], ds[:, 1], label='Punkty')
    
    for degree, ls in [[3, '-'], [4, '-.'], [5, '--'], [6, '-.']]:
        model = make_pipeline(PolynomialFeatures(degree), Ridge(alpha=1e-3))
        model.fit(ds[:, 0][:, np.newaxis], ds[:, 1])
        plt.plot(x, model.predict(x[:, np.newaxis]), ls, label=f'Stopień {degree}')
    
    plt.title('Porównanie wpływu stopnia wielomianu na efekt aproksymacji')
    plt.legend()
    plt.tight_layout()
    plt.show()
    
    x = np.arange(0, 4, 0.01)
    y = func(x)   
    fig, axs = plt.subplots(1, 2, sharey=True)
    
    # First plot with ideal dataset
    ds_ideal = np.append(np.array([.0]), np.random.rand(20), axis=0)
    ds_ideal = np.append(ds_ideal, np.array([1.0]), axis=0)
    ds_ideal = ds_ideal * (np.max(x) - np.min(x)) - np.min(x)
    ds_ideal = np.array(list(zip(ds_ideal, func(ds_ideal))))
    
    model = make_pipeline(PolynomialFeatures(3), Ridge(alpha=1e-3))
    model.fit(ds_ideal[:, 0][:, np.newaxis], ds_ideal[:, 1])
    
    f_interp = interp1d(ds_ideal[:, 0], ds_ideal[:, 1], kind='cubic')
    
    axs[0].plot(x, model.predict(x[:, np.newaxis]), '-.', linewidth=3, label='Aproksymacja')
    axs[0].plot(x, f_interp(x), linewidth=2, label='Interpolacja')
    axs[0].plot(x, y, '--', c='grey', label='Funkcja rzeczywista')
    axs[0].scatter(ds_ideal[:, 0], ds_ideal[:, 1], c='green', label='Punkty należące do funkcji')
    axs[0].set_title('Porównanie dla punktów należących do funkcji')
    axs[0].legend()
    
    # Second plot with measures dataset
    ds_measuers = np.append(np.array([[.0, .0]]), np.random.rand(200, 2), axis=0)
    ds_measuers = np.append(ds_measuers, np.array([[1.0, 1.0]]), axis=0)
    ds_measuers[:, 0] = ds_measuers[:, 0] * (np.max(x) - np.min(x)) - np.min(x)
    ds_measuers[:, 1] = ds_measuers[:, 1] * (np.max(y) - np.min(y)) - np.min(y)
    ds_measuers = ds_measuers[np.abs(func(ds_measuers[:, 0]) - ds_measuers[:, 1]) <= 1]
    
    model = make_pipeline(PolynomialFeatures(3), Ridge(alpha=1e-3))
    model.fit(ds_measuers[:, 0][:, np.newaxis], ds_measuers[:, 1])
    
    f_interp = interp1d(ds_measuers[:, 0], ds_measuers[:, 1], kind='cubic')
    
    axs[1].plot(x, model.predict(x[:, np.newaxis]), '-.', linewidth=3, label='Aproksymacja')
    axs[1].plot(x, f_interp(x), linewidth=2, label='Interpolacja')
    axs[1].plot(x, y, '--', c='grey', label='Funkcja rzeczywista')
    axs[1].scatter(ds_measuers[:, 0], ds_measuers[:, 1], c='green', label='Punkty przybliżające funkcje')
    axs[1].set_title('Porównanie dla punktów przybliżających funkcję')
    axs[1].legend()
    
    plt.tight_layout()
    plt.show()



if __name__ == '__main__':
    main()
