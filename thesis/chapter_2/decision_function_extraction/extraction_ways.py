import numpy as np
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.preprocessing import StandardScaler
from scipy.spatial import Voronoi, voronoi_plot_2d


def main():
    points = np.array([
        [1, 1], [1, 2],       [2, 1.5],   [1, 3],     [2, 2.2],   [3, 1.5],    [4, 1],
        [1.6, 4], [4.5, 3.2], [3.8, 4.1], [3.1, 3.2], [1.5, 3.2], [2.5, 2.2], [3.5, 1.75], [4.5, 1.5]
    ])
    vor = Voronoi(points)
    fig = voronoi_plot_2d(vor, show_points=False, show_vertices=False)
    plt.scatter(points[:7, 0], points[:7, 1], marker='o', label="Punkty klasy A")
    plt.scatter(points[7:, 0], points[7:, 1], marker='x', c='green', label='Punkty klasy B')
    # vor.vertices = vor.vertices[vor.vertices[:, 0] <= 5]
    # vor.vertices = vor.vertices[vor.vertices[:, 0] >= 0]
    # vor.vertices = vor.vertices[vor.vertices[:, 1] >= 0]
    # vor.vertices = vor.vertices[vor.vertices[:, 1] <= 5]
    vor.vertices = vor.vertices[[3, 10, 9, 14, 15, 13, 11, 8], :]
    vor.vertices = np.insert(vor.vertices, 0, np.array([4.8, 0.7]), axis=0)
    vor.vertices = np.insert(vor.vertices, 9, np.array([0.66, 3.88]), axis=0)
    plt.plot(vor.vertices[:, 0], vor.vertices[:, 1], c='red', label='Przybliżona funkcja decyzyjna')
    plt.title('Przybliżenie przebiegu funkcji decyzyjnej metodą diagramu Voronoi\'a')

    plt.legend()
    plt.tight_layout()
    plt.show()

    def function(x):
        return 1/(0.2 + 2.7**(x-5))

    points = np.array([
        [0.5, 1.2], [1.5, 0.5], [3.2, 1.5],  [0.9, 2.8], [0.5, 4.5], [1.75, 4],   [2, 1.7],   [4.5, 1],
        [3, 4.5],   [4, 3.5],   [4.5, 4.5], [4.2, 2.6], [1, 4.75],  [1.95, 4.5], [3.2, 3.5], [4.9, 1.4]
    ])
    plt.scatter(points[:8, 0], points[:8, 1], marker='o', label="Punkty klasy A")
    plt.scatter(points[8:, 0], points[8:, 1], marker='x', c='green', label='Punkty klasy B')

    middle_points = np.array(list(map(
        lambda pair: np.array(pair[0] + pair[1])/2.0, zip(points[4:8, :], points[12:, :])
    )))
    # plt.scatter(middle_points[:, 0], middle_points[:, 1], marker='s', c='red', label='Punkty środkowe')
    plt.plot(middle_points[:, 0], middle_points[:, 1], '-s', c='red', label='Przybliżona funkcja decyzyjna')

    x = np.arange(0, 5, 0.01)
    y = list(map(function, x))
    plt.plot(x, y, c='purple', label='Rzeczywista funkcja decyzyjna')

    for pair in zip(points[4:8, :], points[12:, :]):
        plt.plot([pair[0][0], pair[1][0]], [pair[0][1], pair[1][1]], '--', c='grey')

    plt.title('Przybliżenie przebiegu funkcji decyzyjnej metodą punktów środkowych')
    plt.legend()
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
