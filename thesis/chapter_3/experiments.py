import os
import sys
import pickle

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import tensorflow as tf

from mlxtend.plotting import plot_decision_regions
from sklearn.datasets import make_moons, make_blobs, make_classification
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.datasets import load_iris
from sklearn.decomposition import PCA
from sklearn.manifold import TSNE

from decision_function.distillation import KnowledgeDistillator
from decision_function.extraction import DecisionFunctionExtractor
from decision_function.approximation import FunctionApproximator
from decision_function.adapters import DecisionFunctionsAIModel, SigmoidAIModel, SoftmaxAIModel


class DataSetsGenerator(object):

    def __init__(self):
        pass

    @staticmethod
    def make_moons(n_samples=10000):
        moons = make_moons(n_samples=n_samples, noise=.22, random_state=1)
        x_moons = StandardScaler().fit_transform(moons[0])
        y_moons = moons[1]
        return x_moons, y_moons

    @staticmethod
    def make_blobs(n_samples=10000):
        centers = np.array([[0, 0], [5, 10], [10, 0]])
        blobs = make_blobs(n_samples=n_samples, n_features=2, centers=centers, cluster_std=2.5, random_state=1)
        x_blobs = StandardScaler().fit_transform(blobs[0])
        y_blobs = blobs[1]
        return x_blobs, y_blobs

    @staticmethod
    def make_random_dataset(n_samples=10000, noise=0.25):
        """ Used functions equalities:

        0: -|x| + a
        1: -x^2 + b

        0, 1 => points classes
        a, b => functions coefficients
        """
        a, b = 1, 1
        points_per_point = 10
        ns_samples = [int(n_samples/2), n_samples - int(n_samples/2)]

        y_rand = np.zeros(n_samples)
        y_rand[ns_samples[0]:] = 1

        xs0 = np.linspace(-a, a, int(ns_samples[0] / points_per_point))
        xs1 = np.linspace(-b, b, int(ns_samples[1] / points_per_point))

        xs0 = np.repeat(xs0, points_per_point)
        xs1 = np.repeat(xs1, points_per_point)

        ys0 = -np.abs(xs0) + 1.5
        ys1 = -xs1**2 + .5

        random_noise = np.random.random((n_samples, 2)) * (2*noise) - noise

        x_rand = np.concatenate([
            np.array(list(zip(xs0, ys0))), np.array(list(zip(xs1, ys1)))
        ]) + random_noise

        x_rand = StandardScaler().fit_transform(x_rand)
        return x_rand, y_rand

    @staticmethod
    def plot_datasets_comparison(n_samples=10000):
        x_moons, y_moons = DataSetsGenerator.make_moons(n_samples)
        x_blobs, y_blobs = DataSetsGenerator.make_blobs(n_samples)
        x_rand, y_rand = DataSetsGenerator.make_random_dataset(n_samples)

        plt.subplots_adjust(bottom=0.05, top=0.9, left=0.05, right=0.95)

        ax1 = plt.subplot(131)
        plt.scatter(x_moons[y_moons == 0][:, 0], x_moons[y_moons == 0][:, 1], label='Klasa 0')
        plt.scatter(x_moons[y_moons == 1][:, 0], x_moons[y_moons == 1][:, 1], label='Klasa 1')
        plt.title('Zbiór półksiężyców')
        plt.legend()

        plt.subplot(132, sharey=ax1)
        plt.scatter(x_blobs[y_blobs == 0][:, 0], x_blobs[y_blobs == 0][:, 1], label='Klasa 0')
        plt.scatter(x_blobs[y_blobs == 1][:, 0], x_blobs[y_blobs == 1][:, 1], label='Klasa 1')
        plt.scatter(x_blobs[y_blobs == 2][:, 0], x_blobs[y_blobs == 2][:, 1], label='Klasa 2')
        plt.title('Zbiór plam')
        plt.legend()

        plt.subplot(133, sharey=ax1)
        plt.scatter(x_rand[y_rand == 0][:, 0], x_rand[y_rand == 0][:, 1], label='Klasa 0')
        plt.scatter(x_rand[y_rand == 1][:, 0], x_rand[y_rand == 1][:, 1], label='Klasa 1')
        plt.title('Zbiór punktów funkcji |x| i x^2')
        plt.legend()

        plt.show()

    @staticmethod
    def load_banknote_dataset(n_dim=2):
        df = pd.read_csv(os.path.join('.', 'data', 'banknote.csv'))

        labels = ['skewness', 'variance', 'entropy', 'curtosis']

        if n_dim == 2:
            labels = ['skewness', 'variance']

        x_bank = df[labels].to_numpy()
        y_bank = df['class'].to_numpy()

        x_bank = StandardScaler().fit_transform(x_bank)

        return x_bank, y_bank

    @staticmethod
    def load_iris_dataset():
        iris = load_iris()

        x_iris, y_iris = iris['data'], iris['target']

        x_iris = x_iris[:, [2, 3]]
        x_iris = StandardScaler().fit_transform(x_iris)

        return x_iris, y_iris


    @staticmethod
    def real_problems_datasets_comparison():
        x_bank, y_bank = DataSetsGenerator.load_banknote_dataset()
        x_iris, y_iris = DataSetsGenerator.load_iris_dataset()

        ax1 = plt.subplot(121)
        plt.scatter(x_iris[y_iris == 0][:, 0], x_iris[y_iris == 0][:, 1], label="Klasa 0")
        plt.scatter(x_iris[y_iris == 1][:, 0], x_iris[y_iris == 1][:, 1], label="Klasa 1")
        plt.scatter(x_iris[y_iris == 2][:, 0], x_iris[y_iris == 2][:, 1], label="Klasa 2")
        plt.legend()
        plt.tight_layout()
        plt.title('Zbiór irysów')

        plt.subplot(122, sharey=ax1)
        plt.scatter(x_bank[y_bank == 0][:, 0], x_bank[y_bank == 0][:, 1], label="Klasa 0")
        plt.scatter(x_bank[y_bank == 1][:, 0], x_bank[y_bank == 1][:, 1], label="Klasa 1")
        plt.title('Zbiór banknotów')

        plt.legend()
        plt.tight_layout()
        plt.show()


    @staticmethod
    def train_valid_test_split(*arrays,
                               test_size: float = 0.25, train_size: float = 0.60, valid_size: float = 0.15,
                               shuffle=True, stratify=None):
        x_train, x_test, y_train, y_test = train_test_split(*arrays, test_size=test_size,
                                                            train_size=(train_size + valid_size),
                                                            shuffle=shuffle, stratify=stratify)
        valid_size = valid_size * 1 / (train_size + valid_size)
        x_train, x_valid, y_train, y_valid = train_test_split(x_train, y_train, test_size=valid_size,
                                                              shuffle=shuffle, stratify=y_train)
        return x_train, x_valid, x_test, y_train, y_valid, y_test

    # Downloaded from ML datasets bases
    # banknote dataset
    # iris
    # ...


class Distiller(tf.keras.Model):
    def __init__(self, student, teacher):
        super(Distiller, self).__init__()
        self.teacher = teacher
        self.student = student

    def compile(
        self,
        optimizer,
        metrics,
        student_loss_fn,
        distillation_loss_fn,
        alpha=0.1,
        temperature=3,
    ):
        """ Configure the distiller.

        Args:
            optimizer: Keras optimizer for the student weights
            metrics: Keras metrics for evaluation
            student_loss_fn: Loss function of difference between student
                predictions and ground-truth
            distillation_loss_fn: Loss function of difference between soft
                student predictions and soft teacher predictions
            alpha: weight to student_loss_fn and 1-alpha to distillation_loss_fn
            temperature: Temperature for softening probability distributions.
                Larger temperature gives softer distributions.
        """
        super(Distiller, self).compile(optimizer=optimizer, metrics=metrics)
        self.student_loss_fn = student_loss_fn
        self.distillation_loss_fn = distillation_loss_fn
        self.alpha = alpha
        self.temperature = temperature

    def train_step(self, data):
        # Unpack data
        x, y = data

        # Forward pass of teacher
        teacher_predictions = self.teacher(x, training=False)

        with tf.GradientTape() as tape:
            # Forward pass of student
            student_predictions = self.student(x, training=True)

            # Compute losses
            student_loss = self.student_loss_fn(y, student_predictions)

            # Compute scaled distillation loss from https://arxiv.org/abs/1503.02531
            # The magnitudes of the gradients produced by the soft targets scale
            # as 1/T^2, multiply them by T^2 when using both hard and soft targets.
            distillation_loss = (
                self.distillation_loss_fn(
                    tf.nn.softmax(teacher_predictions / self.temperature, axis=1),
                    tf.nn.softmax(student_predictions / self.temperature, axis=1),
                )
                * self.temperature**2
            )

            loss = self.alpha * student_loss + (1 - self.alpha) * distillation_loss

        # Compute gradients
        trainable_vars = self.student.trainable_variables
        gradients = tape.gradient(loss, trainable_vars)

        # Update weights
        self.optimizer.apply_gradients(zip(gradients, trainable_vars))

        # Update the metrics configured in `compile()`.
        self.compiled_metrics.update_state(y, student_predictions)

        # Return a dict of performance
        results = {m.name: m.result() for m in self.metrics}
        results.update(
            {"student_loss": student_loss, "distillation_loss": distillation_loss}
        )
        return results

    def test_step(self, data):
        # Unpack the data
        x, y = data

        # Compute predictions
        y_prediction = self.student(x, training=False)

        # Calculate the loss
        student_loss = self.student_loss_fn(y, y_prediction)

        # Update the metrics.
        self.compiled_metrics.update_state(y, y_prediction)

        # Return a dict of performance
        results = {m.name: m.result() for m in self.metrics}
        results.update({"student_loss": student_loss})
        return results



def make_moons_experiments():
    test_accuracies = {'teacher': [], 'student1': [], 'student2': [], 'smoothing': []}
    train_accuracies = {'teacher': [], 'student1': [], 'student2': [], 'smoothing': []}

    x_moons, y_moons = DataSetsGenerator.make_moons(n_samples=5000)
    # plt.scatter(x_moons[y_moons == 0][:, 0], x_moons[y_moons == 0][:, 1])
    # plt.scatter(x_moons[y_moons == 1][:, 0], x_moons[y_moons == 1][:, 1])
    # plt.show()

    for iteration in range(12):

        x_train, x_valid, x_test, y_train, y_valid, y_test = DataSetsGenerator.train_valid_test_split(x_moons, y_moons, stratify=y_moons)
        no_epochs = 200

        teacher = tf.keras.models.Sequential(
            [
                tf.keras.layers.Dense(128, activation='relu'),
                tf.keras.layers.Dense(256, activation='relu'),
                tf.keras.layers.Dense(512, activation='relu'),
                tf.keras.layers.Dense(1, activation='sigmoid')
            ],
            name='teacher'
        )
        teacher.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
        history = teacher.fit(x_train, y_train, validation_data=(x_valid, y_valid), epochs=no_epochs).history
        # teacher.save('teacher.teacher')

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        score = accuracy_score(y_test, SigmoidAIModel(teacher).predict(x_test))
        print('Teacher, test =>', score)
        test_accuracies['teacher'].append(score)

        score = accuracy_score(y_train, SigmoidAIModel(teacher).predict(x_train))
        print('Teacher, train =>', score)
        train_accuracies['teacher'].append(score)

        # plot_decision_regions(x_test, y_test, SigmoidAIModel(teacher))
        # plt.show()

        approx_funcs = []
        extraction = DecisionFunctionExtractor(teacher, precision=0.01).fit(x_train, y_train).extract()
        for clazz, points in extraction.classes_points.items():
            clazz = int(clazz)
            approx_func = FunctionApproximator(15, alpha=0.01, points_class=clazz,
                                               inequalities=extraction.classes_inequalities[clazz])
            approx_funcs.append(approx_func.fit(points).approximate())
        model = DecisionFunctionsAIModel(approx_funcs)
        with open('smoothing.pkl', 'wb') as file:
            pickle.dump(model, file)

        score = accuracy_score(y_test, model.predict(x_test))
        print('Smoothing, test =>', score)
        test_accuracies['smoothing'].append(score)

        score = accuracy_score(y_train, model.predict(x_train))
        print('Smoothing, train =>', score)
        train_accuracies['smoothing'].append(score)

        # plot_decision_regions(x_test, y_test, model)
        # plt.show()

        # Knowledge distillation
        student = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(10, activation='relu'),
                tf.keras.layers.Dense(15, activation='relu'),
                tf.keras.layers.Dense(1, activation='sigmoid')
            ],
            name='student',
        )
        no_epochs = 100
        distiller = Distiller(student=student, teacher=teacher)
        distiller.compile(
            optimizer=tf.keras.optimizers.Adam(),
            metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy')],
            student_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            distillation_loss_fn=tf.keras.losses.KLDivergence()
        )
        history = distiller.fit(x_train, y_train, epochs=no_epochs, validation_data=(x_valid, y_valid)).history
        # student.save('student.student')

        score = accuracy_score(y_test, SigmoidAIModel(student).predict(x_test))
        print('Student2, test =>', score)
        test_accuracies['student2'].append(score)

        score = accuracy_score(y_train, SigmoidAIModel(student).predict(x_train))
        print('Student2, train =>', score)
        train_accuracies['student2'].append(score)

        # student = tf.keras.Sequential(
        #     [
        #         tf.keras.layers.Dense(100, activation='relu'),
        #         tf.keras.layers.Dense(1, activation='sigmoid')
        #     ],
        #     name='student',
        # )
        # distiller = Distiller(student=student, teacher=teacher)
        # distiller.compile(
        #     optimizer=tf.keras.optimizers.Adam(),
        #     metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy')],
        #     student_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
        #     distillation_loss_fn=tf.keras.losses.KLDivergence()
        # )
        # history = distiller.fit(x_train, y_train, epochs=no_epochs, validation_data=(x_valid, y_valid)).history
        # # student.save('student.student')
        #
        # score = accuracy_score(y_test, SigmoidAIModel(student).predict(x_test))
        # print('Student1, test =>', score)
        # test_accuracies['student1'].append(score)
        #
        # score = accuracy_score(y_train, SigmoidAIModel(student).predict(x_train))
        # print('Student1, train =>', score)
        # train_accuracies['student1'].append(score)

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        fig, axs = plt.subplots(nrows=1, ncols=3, sharey='all')

        plot_decision_regions(x_test, y_test, SigmoidAIModel(teacher), ax=axs[0], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        axs[0].set_title('Dystylacja, nauczyciel')
        axs[0].set_xlim([-2, 2])
        axs[0].set_ylim([-1.25, 1.25])

        plot_decision_regions(x_test, y_test, SigmoidAIModel(student), ax=axs[1], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        axs[1].set_title('Dystylacja, student')
        axs[1].set_xlim([-2, 2])
        axs[1].set_ylim([-1.25, 1.25])

        plot_decision_regions(x_test, y_test, model, ax=axs[2], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        axs[2].set_title('Wygładzanie przestrzeni decyzyjnych')
        axs[2].set_xlim([-2, 2])
        axs[2].set_ylim([-1.25, 1.25])

        plt.tight_layout(pad=0)
        plt.show()
        break

    print('Student1 test =>', np.mean(test_accuracies['student1']), np.std(test_accuracies['student1']))
    print('Student2 test =>', np.mean(test_accuracies['student2']), np.std(test_accuracies['student2']))
    print('Teacher test =>', np.mean(test_accuracies['teacher']), np.std(test_accuracies['teacher']))
    # print('Smoothing test =>', np.mean(test_accuracies['smoothing']), np.std(test_accuracies['smoothing']))
    print('Student1 train =>', np.mean(train_accuracies['student1']), np.std(train_accuracies['student1']))
    print('Student2 train =>', np.mean(train_accuracies['student2']), np.std(train_accuracies['student2']))
    print('Teacher train =>', np.mean(train_accuracies['teacher']), np.std(train_accuracies['teacher']))
    # print('Smoothing train =>', np.mean(train_accuracies['smoothing']), np.std(train_accuracies['smoothing']))

    print(train_accuracies)
    print(test_accuracies)



def make_blobs_experiments():
    x_blobs, y_blobs = DataSetsGenerator.make_blobs(n_samples=5000)
    # plt.scatter(x_blobs[y_blobs == 0][:, 0], x_blobs[y_blobs == 0][:, 1])
    # plt.scatter(x_blobs[y_blobs == 1][:, 0], x_blobs[y_blobs == 1][:, 1])
    # plt.scatter(x_blobs[y_blobs == 2][:, 0], x_blobs[y_blobs == 2][:, 1])
    # plt.show()

    test_accuracies = {'teacher': [], 'student': [], 'smoothing': []}
    train_accuracies = {'teacher': [], 'student': [], 'smoothing': []}

    for iteration in range(15):

        x_train, x_valid, x_test, y_train, y_valid, y_test = DataSetsGenerator.train_valid_test_split(x_blobs, y_blobs, stratify=y_blobs)
        no_epochs = 200

        teacher = tf.keras.models.Sequential(
            [
                tf.keras.layers.Dense(128, activation='relu'),
                tf.keras.layers.Dense(512, activation='relu'),
                tf.keras.layers.Dense(1024, activation='relu'),
                tf.keras.layers.Dense(3, activation='softmax')
            ],
            name='teacher'
        )
        teacher.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])
        history = teacher.fit(x_train, y_train, validation_data=(x_valid, y_valid), epochs=no_epochs).history
        # teacher.save('teacher.teacher')

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        score = accuracy_score(y_test, SoftmaxAIModel(teacher).predict(x_test))
        print('Teacher, test =>', score)
        test_accuracies['teacher'].append(score)

        score = accuracy_score(y_train, SoftmaxAIModel(teacher).predict(x_train))
        print('Teacher, train =>', score)
        train_accuracies['teacher'].append(score)

        # plot_decision_regions(x_test, y_test, SoftmaxAIModel(teacher))
        # plt.show()

        # approx_funcs = []
        # extraction = DecisionFunctionExtractor(teacher, precision=0.01).fit(x_train, y_train).extract()
        # for clazz, points in extraction.classes_points.items():
        #     clazz = int(clazz)
        #     approx_func = FunctionApproximator(6,
        #                                        alpha=0.01, points_class=clazz,
        #                                        inequalities=extraction.classes_inequalities[clazz])
        #     approx_funcs.append(approx_func.fit(points).approximate())
        # model = DecisionFunctionsAIModel(approx_funcs)
        #
        # with open('smoothing.pkl', 'wb') as file:
        #     pickle.dump(model, file)
        #
        # score = accuracy_score(y_test, model.predict(x_test))
        # print('Smoothing, test =>', score)
        # test_accuracies['smoothing'].append(score)
        #
        # score = accuracy_score(y_train, model.predict(x_train))
        # print('Smoothing, train =>', score)
        # train_accuracies['smoothing'].append(score)

        # plot_decision_regions(x_test, y_test, model)
        # plt.show()

        # Knowledge distillation
        student = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(10, activation='relu'),
                tf.keras.layers.Dense(3, activation='softmax')
            ],
            name='student',
        )
        no_epochs = 100
        distiller = Distiller(student=student, teacher=teacher)
        distiller.compile(
            optimizer=tf.keras.optimizers.Adam(),
            metrics=[tf.keras.metrics.SparseCategoricalAccuracy(name='accuracy')],
            student_loss_fn=tf.keras.losses.SparseCategoricalCrossentropy(from_logits=True),
            distillation_loss_fn=tf.keras.losses.KLDivergence()
        )
        history = distiller.fit(x_train, y_train, epochs=no_epochs, validation_data=(x_valid, y_valid)).history
        student.save('student.student')

        score = accuracy_score(y_test, SoftmaxAIModel(student).predict(x_test))
        print('Student, test =>', score)
        test_accuracies['student'].append(score)

        score = accuracy_score(y_train, SoftmaxAIModel(student).predict(x_train))
        print('Student, train =>', score)
        train_accuracies['student'].append(score)

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        # plot_decision_regions(x_test, y_test, SigmoidAIModel(student))
        # plt.show()

        # fig, axs = plt.subplots(nrows=1, ncols=3, sharey='all')
        #
        # plot_decision_regions(x_test, y_test, SoftmaxAIModel(teacher), ax=axs[0], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[0].set_title('Dystylacja, nauczyciel')
        # axs[0].set_xlim([-3, 3])
        # axs[0].set_ylim([-.75, 1.5])
        #
        # plot_decision_regions(x_test, y_test, SoftmaxAIModel(student), ax=axs[1], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[1].set_title('Dystylacja, student')
        # axs[1].set_xlim([-3, 3])
        # axs[1].set_ylim([-.75, 1.5])
        #
        # plot_decision_regions(x_test, y_test, model, ax=axs[2], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[2].set_title('Wygładzanie przestrzeni decyzyjnych')
        # axs[2].set_xlim([-3, 3])
        # axs[2].set_ylim([-.75, 1.5])
        #
        # plt.tight_layout()
        # plt.show()
        #
        break

    print('Student test =>', np.mean(test_accuracies['student']), np.std(test_accuracies['student']))
    print('Teacher test =>', np.mean(test_accuracies['teacher']), np.std(test_accuracies['teacher']))
    # print('Smoothing test =>', np.mean(test_accuracies['smoothing']), np.std(test_accuracies['smoothing']))
    print('Student train =>', np.mean(train_accuracies['student']), np.std(train_accuracies['student']))
    print('Teacher train =>', np.mean(train_accuracies['teacher']), np.std(train_accuracies['teacher']))
    # print('Smoothing train =>', np.mean(train_accuracies['smoothing']), np.std(train_accuracies['smoothing']))

    print(train_accuracies)
    print(test_accuracies)


def make_random_experiments():
    x_rand, y_rand = DataSetsGenerator.make_random_dataset()
    plt.scatter(x_rand[y_rand == 0][:, 0], x_rand[y_rand == 0][:, 1])
    plt.scatter(x_rand[y_rand == 1][:, 0], x_rand[y_rand == 1][:, 1])
    plt.show()



def make_banknotes_experiments():
    x_bank, y_bank = DataSetsGenerator.load_banknote_dataset()

    # plt.scatter(x_bank[y_bank == 0][:, 0], x_bank[y_bank == 0][:, 1], label='Klasa 1')
    # plt.scatter(x_bank[y_bank == 1][:, 0], x_bank[y_bank == 1][:, 1], label='Klasa 2')
    # plt.show()

    test_accuracies = {'teacher': [], 'student1': [], 'student2': [], 'smoothing': []}
    train_accuracies = {'teacher': [], 'student1': [], 'student2': [], 'smoothing': []}

    for iteration in range(12):
        x_train, x_valid, x_test, y_train, y_valid, y_test = DataSetsGenerator.train_valid_test_split(x_bank, y_bank, stratify=y_bank,
                                                                                                      test_size=0.25, train_size=0.65, valid_size=0.10)
        no_epochs = 200
        teacher = tf.keras.models.Sequential(
            [
                tf.keras.layers.Dense(100, activation='relu'),
                tf.keras.layers.Dense(300, activation='relu'),
                tf.keras.layers.Dense(500, activation='relu'),
                tf.keras.layers.Dense(1, activation='sigmoid')
            ],
            name='teacher'
        )
        teacher.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
        history = teacher.fit(x_train, y_train, validation_data=(x_valid, y_valid), epochs=no_epochs).history
        # teacher.save('teacher.teacher')

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        score = accuracy_score(y_test, SigmoidAIModel(teacher).predict(x_test))
        print('Teacher, test =>', score)
        test_accuracies['teacher'].append(score)

        score = accuracy_score(y_train, SigmoidAIModel(teacher).predict(x_train))
        print('Teacher, train =>', score)
        train_accuracies['teacher'].append(score)

        # plot_decision_regions(x_test, y_test, SigmoidAIModel(teacher))
        # plt.show()

        # approx_funcs = []
        # extraction = DecisionFunctionExtractor(teacher, precision=0.05).fit(x_train, y_train).extract()
        # for clazz, points in extraction.classes_points.items():
        #     clazz = int(clazz)
        #
        #     approx_func = FunctionApproximator(21,
        #                                        alpha=0.001, points_class=clazz,
        #                                        inequalities=extraction.classes_inequalities[clazz])
        #     approx_func = approx_func.fit(points).approximate()
        #     approx_funcs.append(approx_func)
        # model = DecisionFunctionsAIModel(approx_funcs)

        # with open('smoothing.pkl', 'wb') as file:
        #     pickle.dump(model, file)

        # score = accuracy_score(y_test, model.predict(x_test))
        # print('Smoothing, test =>', score)
        # test_accuracies['smoothing'].append(score)
        #
        # score = accuracy_score(y_train, model.predict(x_train))
        # print('Smoothing, train =>', score)
        # train_accuracies['smoothing'].append(score)

        # plot_decision_regions(x_test, y_test, model)
        # plt.show()

        # Knowledge distillation
        student = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(100, activation='relu'),
                tf.keras.layers.Dense(1, activation='sigmoid')
            ],
            name='student',
        )
        no_epochs = 120
        distiller = Distiller(student=student, teacher=teacher)
        distiller.compile(
            optimizer=tf.keras.optimizers.Adam(),
            metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy')],
            student_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            distillation_loss_fn=tf.keras.losses.KLDivergence()
        )
        history = distiller.fit(x_train, y_train, epochs=no_epochs, validation_data=(x_valid, y_valid)).history
        # student.save('student.student')

        score = accuracy_score(y_test, SigmoidAIModel(student).predict(x_test))
        print('Student, test =>', score)
        test_accuracies['student1'].append(score)

        score = accuracy_score(y_train, SigmoidAIModel(student).predict(x_train))
        print('Student, train =>', score)
        train_accuracies['student1'].append(score)

        student = tf.keras.Sequential(
            [
                tf.keras.layers.Dense(80, activation='relu'),
                tf.keras.layers.Dense(120, activation='relu'),
                tf.keras.layers.Dense(1, activation='sigmoid')
            ],
            name='student',
        )
        distiller = Distiller(student=student, teacher=teacher)
        distiller.compile(
            optimizer=tf.keras.optimizers.Adam(),
            metrics=[tf.keras.metrics.BinaryAccuracy(name='accuracy')],
            student_loss_fn=tf.keras.losses.BinaryCrossentropy(from_logits=True),
            distillation_loss_fn=tf.keras.losses.KLDivergence()
        )
        history = distiller.fit(x_train, y_train, epochs=no_epochs, validation_data=(x_valid, y_valid)).history
        # student.save('student.student')

        score = accuracy_score(y_test, SigmoidAIModel(student).predict(x_test))
        print('Student, test =>', score)
        test_accuracies['student2'].append(score)

        score = accuracy_score(y_train, SigmoidAIModel(student).predict(x_train))
        print('Student, train =>', score)
        train_accuracies['student2'].append(score)

        # plt.plot(range(no_epochs), history['accuracy'], label='dokładność uczenia')
        # plt.plot(range(no_epochs), history['val_accuracy'], label='dokładność walidacji')
        # plt.legend()
        # plt.tight_layout()
        # plt.show()

        # plot_decision_regions(x_test, y_test, SigmoidAIModel(student))
        # plt.show()

        # fig, axs = plt.subplots(nrows=1, ncols=3, sharey='all')
        #
        # plot_decision_regions(x_test, y_test, SigmoidAIModel(teacher), ax=axs[0],
        #                       scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[0].set_title('Dystylacja, nauczyciel')
        # axs[0].set_xlim([-2.8, 2.2])
        # axs[0].set_ylim([-2.8, 2])
        #
        # plot_decision_regions(x_test, y_test, SigmoidAIModel(student), ax=axs[1],
        #                       scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[1].set_title('Dystylacja, student')
        # axs[1].set_xlim([-2.8, 2.2])
        # axs[1].set_ylim([-2.8, 2])
        #
        # plot_decision_regions(x_test, y_test, model, ax=axs[2], scatter_kwargs={'label': ['Klasa 0', 'Klasa 1']})
        # axs[2].set_title('Wygładzanie przestrzeni decyzyjnych')
        # axs[2].set_xlim([-2.8, 2.2])
        # axs[2].set_ylim([-2.8, 2])
        #
        # plt.tight_layout()
        # plt.show()

        # break

    print('Student1 test =>', np.mean(test_accuracies['student1']), np.std(test_accuracies['student1']))
    print('Student2 test =>', np.mean(test_accuracies['student2']), np.std(test_accuracies['student2']))
    print('Teacher test =>', np.mean(test_accuracies['teacher']), np.std(test_accuracies['teacher']))
    # print('Smoothing test =>', np.mean(test_accuracies['smoothing']), np.std(test_accuracies['smoothing']))
    print('Student1 train =>', np.mean(train_accuracies['student1']), np.std(train_accuracies['student1']))
    print('Student2 train =>', np.mean(train_accuracies['student2']), np.std(train_accuracies['student2']))
    print('Teacher train =>', np.mean(train_accuracies['teacher']), np.std(train_accuracies['teacher']))
    # print('Smoothing train =>', np.mean(train_accuracies['smoothing']), np.std(train_accuracies['smoothing']))

    print(train_accuracies)
    print(test_accuracies)


if __name__ == '__main__':
    DataSetsGenerator.plot_datasets_comparison()
    DataSetsGenerator.real_problems_datasets_comparison()

    make_moons_experiments()
    make_blobs_experiments()
    make_random_experiments()
    make_banknotes_experiments()
