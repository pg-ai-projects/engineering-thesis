import numpy as np
import matplotlib.pyplot as plt


def main():
    points = np.array([
        [0, 0.3], [0.1, 0.3], [0.2, 0.3], [0, 0.2], [0.1, 0.2], [0, 0.1], [0.1, 0.1], [0, 0], [0.1, 0], [0.2, 0], [0.3, 0.1],
        [0.3, 0.3], [0.2, 0.2], [0.3, 0.2], [0.2, 0.1], [0.3, 0]
    ])

    plt.scatter(points[:10, 0], points[:10, 1], label='Punkty klasy 0')
    plt.scatter(points[10:, 0], points[10:, 1], marker='x', c='green', label='Punkty klasy 1')

    p = np.array([
        [0.25, 0.3], [0.15, 0.2], [0.15, 0.1], [0.25, 0]
    ])
    plt.plot(p[:, 0], p[:, 1], '-o', c='red', label='Wyznaczona funkcja decyzyjna')

    # p2 = np.array([
    #     [0.25, 0.3], [0.2, 0.25], [0.15, 0.2], [0.15, 0.1], [0.2, 0.05], [0.25, 0]
    # ])
    # plt.plot(p2[:, 0], p2[:, 1], '--o', c='gray')

    plt.title('Efekt wyznaczenia przykładowej funkcji decyzyjnej')
    plt.legend()
    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()
