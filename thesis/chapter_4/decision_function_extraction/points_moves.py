import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

from sklearn.datasets import make_moons
from sklearn.preprocessing import StandardScaler


def main():
    moons = make_moons(n_samples=500, noise=.2, random_state=1)
    x_moons = StandardScaler().fit_transform(moons[0])
    y_moons = moons[1]

    teacher = tf.keras.models.Sequential()
    teacher.add(tf.keras.layers.Dense(16, activation='relu'))
    teacher.add(tf.keras.layers.Dense(48, activation='relu'))
    teacher.add(tf.keras.layers.Dense(1, activation='sigmoid'))
    teacher.compile(optimizer='adam', loss='binary_crossentropy', metrics=['accuracy'])
    teacher.fit(x_moons, y_moons, epochs=100)

    extraction = DecisionFunctionExtractor(teacher, precision=0.1).fit(x_moons, y_moons).extract()

    funcs = []
    for clazz, points in extraction.classes_points.items():
        clazz = int(clazz)
        funcs.append(
            FunctionApproximator(degree=3,
                                 alpha=1,
                                 points_class=clazz,
                                 inequalities=extraction.classes_inequalities[clazz]).fit(points).approximate()
        )
        break

    func = funcs[0]
    arrow_width = 0.01
    arrow_head_width = .1
    xs = np.arange(-3.5, 3.5, 0.01)
    ys = np.arange(-3.1, 3.1, 0.01)
    xx1, xx2 = np.meshgrid(np.arange(min(xs), max(xs), 0.01),
                           np.arange(min(ys), max(ys), 0.01))

    Z = func.is_class(np.array([xx1.ravel(), xx2.ravel()]).T).astype(int)
    Z = Z.reshape(xx1.shape)

    point1_x, point2_x, point1_y, point2_y = -2, 1.5, 1.5, -.5

    plt.contourf(xx1, xx2, Z, alpha=0.4, cmap=ListedColormap(['#1f77b4', '#ff7f0e']))

    func_ys = func(xs)
    diffs1 = np.abs(func_ys - point1_y)
    idx1 = np.where(diffs1 == diffs1.min())[0]

    diffs2 = np.abs(func_ys - point2_y)
    idx2 = np.where(diffs2 == diffs2.min())[0]

    plt.plot([point1_x, point1_x], [point1_y, func([point1_x])[0]], '--', c='grey')
    plt.plot([point1_x, xs[idx1]], [point1_y, func_ys[idx1]], '--', c='grey')
    plt.plot([point2_x, point2_x], [point2_y, func([point2_x])[0]], '--', c='grey')
    plt.plot([point2_x, xs[idx2]], [point2_y, func_ys[idx2]], '--', c='grey')

    plt.scatter(xs, func(xs), label='Punkty funkcji decyzyjnej', c='maroon')
    plt.scatter([point1_x, point2_x], [point1_y, point2_y], label='Rozpatrywane punkty', marker='s', s=75, c='green')
    plt.scatter([point1_x, xs[idx1], point2_x, xs[idx2]],
                [func([point1_x])[0], func_ys[idx1], func([point2_x])[0], func_ys[idx2]],
                label='Punkt rzutowane względem wymiarów', marker='*', s=100, c='gold')

    plt.title('Sprawdzenie, czy konkretny punkt należy do klasy funkcji')
    plt.ylim([-3, 3])
    plt.xlim([-3, 3])
    plt.legend()
    plt.show()

    fig, axs = plt.subplots(1, 2, sharey='row')

    arrows_xs = np.array([-2, -1, 1, 2])
    arrows_ys = func(arrows_xs)

    axs[0].contourf(xx1, xx2, Z, alpha=0.4, cmap=ListedColormap(['#1f77b4', '#ff7f0e']))
    axs[0].scatter(xs, func(xs), label='Wyekstrahowane punkty', c='black')
    axs[0].scatter(xs + 1, func(xs), label='Przesunięcie o 1', c='purple')
    axs[0].scatter(xs - 1, func(xs), label='Przesunięcie o -1', c='green')
    axs[0].arrow(arrows_xs[0], arrows_ys[0], -.8, 0, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[0].arrow(arrows_xs[1], arrows_ys[1], -.8, 0, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[0].arrow(arrows_xs[2], arrows_ys[2], -.8, 0, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[0].arrow(arrows_xs[3], arrows_ys[3], -.8, 0, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[0].arrow(arrows_xs[0], arrows_ys[0], .8, 0, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[0].arrow(arrows_xs[1], arrows_ys[1], .8, 0, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[0].arrow(arrows_xs[2], arrows_ys[2], .8, 0, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[0].arrow(arrows_xs[3], arrows_ys[3], .8, 0, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[0].set_title('Przesunięcie wyekstrahowanych punktów względem osi OX')
    axs[0].set_ylim([-3, 3])
    axs[0].set_xlim([-3, 3])
    axs[0].legend()

    axs[1].contourf(xx1, xx2, Z, alpha=0.4, cmap=ListedColormap(['#1f77b4', '#ff7f0e']))
    axs[1].scatter(xs, func(xs), label='Wyekstrahowane punkty', c='black')
    axs[1].scatter(xs, func(xs) + 1, label='Przesunięcie o 1', c='purple')
    axs[1].scatter(xs, func(xs) - 1, label='Przesunięcie o -1', c='green')
    axs[1].arrow(arrows_xs[0], arrows_ys[0], 0, -.8, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[1].arrow(arrows_xs[1], arrows_ys[1], 0, -.8, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[1].arrow(arrows_xs[2], arrows_ys[2], 0, -.8, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[1].arrow(arrows_xs[3], arrows_ys[3], 0, -.8, head_width=arrow_head_width, width=arrow_width, color='green')
    axs[1].arrow(arrows_xs[0], arrows_ys[0], 0, .8, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[1].arrow(arrows_xs[1], arrows_ys[1], 0, .8, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[1].arrow(arrows_xs[2], arrows_ys[2], 0, .8, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[1].arrow(arrows_xs[3], arrows_ys[3], 0, .8, head_width=arrow_head_width, width=arrow_width, color='purple')
    axs[1].set_title('Przesunięcie wyekstrahowanych punktów względem osi OY')
    axs[1].set_ylim([-3, 3])
    axs[1].set_xlim([-3, 3])
    axs[1].legend()

    plt.tight_layout()
    plt.show()


if __name__ == '__main__':
    main()