import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from sklearn.datasets import load_iris
from sklearn.preprocessing import StandardScaler

from reducing_dimensionality_algorithms import pca_algorithm, tSNE_algorithm
from plotting_algorithms import plot_decision_regions

iris = load_iris()
x = iris['data']
y = iris['target']

x = StandardScaler().fit_transform(pca_algorithm(x))

x_train = np.empty((120, 2))
x_test = np.empty((30, 2))
y_train = np.empty(120)
y_test = np.empty(30)

x_train[:40], x_train[40:80], x_train[80:120] = x[:40], x[50:90], x[100:140]
x_test[:10], x_test[10:20], x_test[20:30] = x[40:50], x[90:100], x[140:150]
y_train[:40], y_train[40:80], y_train[80:120] = y[:40], y[50:90], y[100:140]
y_test[:10], y_test[10:20], y_test[20:30] = y[40:50], y[90:100], y[140:150]

# TRAINING

model = tf.keras.models.Sequential()

model.add(tf.keras.layers.Dense(16, activation='softmax'))

model.add(tf.keras.layers.Dense(48, activation='relu'))

model.add(tf.keras.layers.Dropout(0.5))

model.add(tf.keras.layers.Dense(3, activation='softmax'))

model.compile(optimizer='adam', loss='sparse_categorical_crossentropy', metrics=['accuracy'])

model.fit(x_train, y_train, epochs=100)

model.save('model.model')

print("Model saved\n")

print(model.summary())

# TESTING

predictions = model.predict(x_test[:30])

error = 0

for i in range(len(predictions)):
    guess = np.argmax(predictions[i])
    actual = y_test[i]
    if guess != actual:
        error += 1

print("\nAccuracy = " + str(len(predictions) - error) + "/" + str(len(predictions)))
print("\nAccuracy = " + str(((len(predictions) - error) / len(predictions)) * 100) + "%")

plot_decision_regions(model, x_test, y_test.astype(np.int32), kind='mlxtend')
plt.title('Mlxtend decision regions')
plt.show()

plot_decision_regions(model, x_test, y_test.astype(np.int32), kind='selfcoded')
plt.title('Selfcoded decision regions')
plt.show()
