from sklearn.decomposition import PCA
from sklearn.manifold import TSNE
from sklearn.preprocessing import StandardScaler


def pca_algorithm(data):
    """
    Converts n-dimensional vectors into 2-dimensional vectors using the PCA algorithm.
    Parameters
    ----------
        data : ndarray
            each column shows a feature and each row is an array of values for these features
    """
    data = StandardScaler().fit_transform(data)
    pca = PCA(n_components=2)
    return pca.fit_transform(data)


def tSNE_algorithm(data):
    """
    Converts n-dimensional vectors into 2-dimensional vectors using the t-SNE algorithm.
    Parameters
    ----------
        data : ndarray
            each column shows a feature and each row is an array of values for these features
    """
    data = StandardScaler().fit_transform(data)
    tSNE = TSNE(n_components=2)
    return tSNE.fit_transform(data)
