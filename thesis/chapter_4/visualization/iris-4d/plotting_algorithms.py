from collections import OrderedDict

import mlxtend
import numpy as np
from mlxtend.plotting import plot_decision_regions as mlxtend_regions


def plot_decision_regions(clf, x: np.ndarray, y: np.ndarray, pair: list, kind: str = 'mlxtend'):
    """ Plot decision regions for provided classifier,
    input dataset and its labels.

    Parameters
    ----------
    clf : Classifier
        Classifier object that could predict classes.

    x : ndarray
        Input array for provided classifier.

    y : ndarray
        Labels of `x` parameter rows.

    kind : str optional. Default = 'mlxtend'
        Type of used plotting library.

    Notes
    -----
    Function draw decision regions of provided classifier on new figure.
    It is needed to invoke .show() method of matplotlib.pyplot library to show created plot.
    Before showing plot it is possible to set title, xlabel or ylabel of plot.
    """
    plotters = OrderedDict({
        'mlxtend': _plot_mlxtend_decision_regions
    })

    if not isinstance(kind, str):
        raise TypeError(f'{type(kind)} is inappropriate type of t parameter.')

    kind = kind.lower()
    if not (kind in plotters.keys()):
        raise ValueError(f'{kind} is not allowed type of regions plotter. Allowed values are {list(plotters.keys())}')

    plotter = plotters.get(kind)
    plotter(pair, clf, x, y)


def _plot_mlxtend_decision_regions(pair: list, clf, x: np.ndarray, y: np.ndarray):
    """ Plot decision regions using provided classifier object.
    As drawing algorithm uses function in mlxtend library.

    Parameters
    ----------
    clf : Classifier
        Classifier which is used for predictions. Could be list of classifiers.
    x : ndarray
        List of coordinates list or tuples (x, y) when x and y
        are coordinates in euclides space.
    y : ndarray {default: None}
        List of labels belongs to input X tuples/lists. Could be None.
    """
    value = 0
    width = 10

    ffv = {}
    ffr = {}

    for i in range(4):
        if i != pair[0] and i != pair[1]:
            ffv[i] = value
            ffr[i] = width

    ai_model = AIProbsModel(clf, np.unique(y).tolist())
    mlxtend.plotting.plot_decision_regions(x, y, ai_model, feature_index=[pair[0], pair[1]],
                                           filler_feature_values=ffv,
                                           filler_feature_ranges=ffr,
                                           zoom_factor=0.25)


class AIProbsModel(object):
    """ Class making easier classes prediction
    by model which returns its probabilities.
    """

    def __init__(self, model, classes: list):
        """ Initialize the AIProbsModel object.

        Parameters
        ----------
        model : Classifier
            Classifier which could predict probabilities of classes.

        classes : list
            List of classes which could be predicted.
        """
        self._model = model
        self.classes = classes

    def predict(self, x: np.ndarray):
        """ Predict class of provided x dataset. """
        probs = self._model.predict(x).tolist()
        return np.array(list(map(
            lambda prob: self.classes[prob.index(max(prob))], probs)
        ))
