from collections import OrderedDict
from itertools import cycle, islice

import matplotlib.pyplot as plt
import mlxtend
import numpy as np
from matplotlib.colors import ListedColormap
from mlxtend.plotting import plot_decision_regions as mlxtend_regions


def plot_decision_regions(clf, x: np.ndarray, y: np.ndarray, kind: str = 'mlxtend'):
    """ Plot decision regions for provided classifier,
    input dataset and its labels.

    Parameters
    ----------
    clf : Classifier
        Classifier object that could predict classes.

    x : ndarray
        Input array for provided classifier.

    y : ndarray
        Labels of `x` parameter rows.

    kind : str optional. Default = 'mlxtend'
        Type of used plotting library.

    Notes
    -----
    Function draw decision regions of provided classifier on new figure.
    It is needed to invoke .show() method of matplotlib.pyplot library to show created plot.
    Before showing plot it is possible to set title, xlabel or ylabel of plot.
    """
    plotters = OrderedDict({
        'mlxtend': _plot_mlxtend_decision_regions,
        'selfcoded': _plot_selfcoded_decision_regions
    })

    if len(x.shape) != 2 or x.shape[1] != 2:
        raise ValueError(f'Incorrect shape of input x array')

    if not isinstance(kind, str):
        raise TypeError(f'{type(kind)} is inappropriate type of t parameter.')

    kind = kind.lower()
    if not (kind in plotters.keys()):
        raise ValueError(f'{kind} is not allowed type of regions plotter. Allowed values are {list(plotters.keys())}')

    plotter = plotters.get(kind)
    plotter(clf, x, y)


def _plot_mlxtend_decision_regions(clf, x: np.ndarray, y: np.ndarray):
    """ Plot decision regions using provided classifier object.
    As drawing algorithm uses function in mlxtend library.

    Parameters
    ----------
    clf : Classifier
        Classifier which is used for predictions. Could be list of classifiers.
    x : ndarray
        List of coordinates list or tuples (x, y) when x and y
        are coordinates in euclides space.
    y : ndarray {default: None}
        List of labels belongs to input X tuples/lists. Could be None.
    """
    ai_model = AIProbsModel(clf, np.unique(y).tolist())
    mlxtend.plotting.plot_decision_regions(x, y, ai_model)


def _plot_selfcoded_decision_regions(clf, x: np.ndarray, y: np.ndarray = None):
    """ Plot decision regions using
    provided classifier object.

    Parameters
    ----------
    clf : Classifier
        Classifier which is used for predictions. Could be list of classifiers.
    x : ndarray
        List of coordinates list or tuples (x, y) when x and y
        are coordinates in euclides space.
    y : ndarray {default: None}
        List of labels belongs to input X tuples/lists. Could be None.
    """
    # create list of unique classes on which X elements can belong
    if y is None or len(y) != len(x):
        y = np.array(list(map(lambda item: item.index(max(item)), clf.predict(x).tolist())))

    classes = np.unique(y)
    N = len(classes)

    # plotting decision regions - regions to which points are classified
    # by a given classifier. Different colors are used for each region
    colors = _get_colors(N, kind="str")
    cmap = ListedColormap(colors)

    x1_min, x1_max = x[:, 0].min() - 1, x[:, 0].max() + 1
    x2_min, x2_max = x[:, 1].min() - 1, x[:, 1].max() + 1
    resolution = 0.01
    xx1, xx2 = np.meshgrid(np.arange(x1_min, x1_max, resolution),
                               np.arange(x2_min, x2_max, resolution))

    Z = clf.predict(np.array([xx1.ravel(), xx2.ravel()]).T)

    # if result of prediction is list of probabilities of belongs - reduce
    # these list to indices of max value of belong probability
    if isinstance(Z[0], np.ndarray) or isinstance(Z[0], list) or isinstance(Z[0], tuple):
        Z = np.array(list(map(lambda item: item.index(max(item)), Z.tolist())))

    Z = Z.reshape(xx1.shape)
    plt.contourf(xx1, xx2, Z, alpha=0.3, cmap=cmap)

    # creating and plotting point using different markers and
    # markers colors for all (X data) labels
    markers = _get_markers(N)
    colors = _get_colors(N)

    for i, clazz in enumerate(classes):
        Xs = x[y == clazz][:, 0]
        Ys = x[y == clazz][:, 1]
        plt.scatter(Xs, Ys, marker=markers[i], cmap=colors[i])


def _get_markers(count: int = 1, random: bool = False):
    """ Get list of markers which can be used during plotting
    charts or diagrams in matplotlib package.

    Parameters
    ----------
    n : int {default: 1}
        Number of needed marker. Has to be >=1.
    random : bool {default: False}
        Are markers have to be given randomly.

    References
    ----------
    [1] https://matplotlib.org/stable/api/markers_api.html#module-matplotlib.markers

    Returns
    -------
    markers
        List of markers ready to use in matplotlib package.
    """
    markers_list = cycle(['o', 'v', '^', '<', '>', '1', '2', '3', '4', '8', 's', 'p',
                          '*', 'h', 'H', '+', 'x', 'D', 'd', '|', '_', 'P', 'X'])
    markers = list(islice(markers_list, count))
    if random:
        np.random.shuffle(markers)
    return markers[:count]


def _get_colors(count: int = 1, kind: str = "float", random: bool = False):
    """ Get list of colors which can be used during plotting
    charts or diagrams in matplotlib package

    Parameters
    ----------
    count : int {default: 1}
        number of needed marker. Has to be >=1.
    kind : str {default: "float"}
        Data type (in string) whose colors will be returned.
    random : bool {default: False}
        Are colors have to be given randomly.

    Returns
    -------
    colors
        List of colors ready to use in matplotlib package
    """
    colors_str = cycle(["blue", "orange", "green", "red", "purple", "brown", "pink", "gray", "olive", "cyan",
                        "rosybrown", "goldenrod", "aquamarine", "darkslategrey", "skyblue", "magenta", "indigo",
                        "crimson"])

    if kind in ("int", "float"):
        colors = np.linspace(0, 1, count)
    elif kind in ("str",):
        colors = list(islice(colors_str, count))
    else:
        colors = None

    if random and colors is not None:
        np.random.shuffle(colors)

    return colors


class AIProbsModel(object):
    """ Class making easier classes prediction
    by model which returns its probabilities.
    """

    def __init__(self, model, classes: list):
        """ Initialize the AIProbsModel object.

        Parameters
        ----------
        model : Classifier
            Classifier which could predict probabilities of classes.

        classes : list
            List of classes which could be predicted.
        """
        self._model = model
        self.classes = classes

    def predict(self, x: np.ndarray):
        """ Predict class of provided x dataset. """
        probs = self._model.predict(x).tolist()
        return np.array(list(map(
            lambda prob: self.classes[prob.index(max(prob))], probs)
        ))
